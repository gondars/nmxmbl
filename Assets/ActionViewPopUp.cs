﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ActionViewPopUp : DynamicPopUp
{
    private Action a1;
    private Action a2;
    private string but1Text;
    private string but2Text;

    private Vector2 buttonSize = new Vector2(Screen.width * .24f, Screen.width * .08f);
    private float textWidth = Screen.width * .6f;
    private float butPadding = Screen.width * .02f;
    private float verticalPadding = 0.02f;

    public string DescriptionText { get; set; }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnEnable()
    {

    }


    public override void Create()
    {
        base.Create();
        base.SetName("Action Popup");

        AddRow();
        AddDescription();
        FinishRow();
        AddVerticalPadding(0.05f);

        AddRow();
        AddButtons();
        FinishRow();

        GetBounds(mainContainer);
        SetBackground();
        FinishDrawing();
    }

    protected override void AddDescription()
    {
        GameObject descText = new GameObject("Description");
        descText.AddComponent<RectTransform>();
        descText.AddComponent<CanvasRenderer>();
        Text text = descText.AddComponent<Text>();
        text.text = DescriptionText;
        text.alignment = TextAnchor.MiddleCenter;
        text.font = Resources.Load<Font>("Fonts/PlayRegular");
        text.fontSize = 20;

        descText.GetComponent<RectTransform>().sizeDelta = new Vector2(textWidth, 0);

        ContentSizeFitter fitter = descText.AddComponent<ContentSizeFitter>();
        fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        fitter.SetLayoutVertical();

        AddRowElement(descText);
        descText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
    }

    public void SetButtonsCallbacks(Action callback1 = null, Action callback2 = null)
    {
        a1 = callback1;
        a2 = callback2;
    }


    public void SetButtonsNames(string n1 = "", string n2 = "")
    {
        but1Text = n1;
        but2Text = n2;
    }


    private void AddButtons()
    {
        if (a1 != null) AddFirstButton();
        if (a2 != null) AddSecondButton();
        AddCloseButton();
    }


    private void AddFirstButton()
    {
        GameObject go = new GameObject();
        go.name = but1Text;

        Button btn = go.AddComponent<Button>();
        int id = 1;
        btn.onClick.AddListener(() => OnButtonClicked(id));

        Image img = go.AddComponent<Image>();
        img.sprite = Resources.Load("images/nmxBigButtonBlue", typeof(Sprite)) as Sprite;

        GameObject textGO = new GameObject();
        textGO.transform.SetParent(go.transform);
        Text txt = textGO.gameObject.AddComponent<Text>();
        txt.text = but1Text;
        txt.alignment = TextAnchor.MiddleCenter;
        txt.font = Resources.Load<Font>("Fonts/PlayRegular");
        txt.color = Color.white;
        txt.fontSize = 24;

        go.GetComponent<RectTransform>().sizeDelta = buttonSize;
        //AddComponent(go);
        AddRowElement(go);
        
        go.GetComponent<RectTransform>().anchoredPosition = new Vector2(LineX, 0);
        LineX += buttonSize.x + butPadding;
    }


    private void AddSecondButton()
    {
        GameObject go = new GameObject();
        go.name = but2Text;

        Button btn = go.AddComponent<Button>();
        int id = 2;
        btn.onClick.AddListener(() => OnButtonClicked(id));

        Image img = go.AddComponent<Image>();
        img.sprite = Resources.Load("images/nmxBigButtonBlue", typeof(Sprite)) as Sprite;

        GameObject textGO = new GameObject();
        textGO.transform.SetParent(go.transform);
        Text txt = textGO.gameObject.AddComponent<Text>();
        txt.text = but2Text;
        txt.alignment = TextAnchor.MiddleCenter;
        txt.font = Resources.Load<Font>("Fonts/PlayRegular");
        txt.color = Color.white;
        txt.fontSize = 24;

        go.GetComponent<RectTransform>().sizeDelta = buttonSize;
        //AddComponent(go);
        AddRowElement(go);

        float x = buttonSize.x + butPadding;
        float y = 0f;
        go.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);

        LineX += buttonSize.x + butPadding;
    }


    // cancel button
    private void AddCloseButton()
    {
        GameObject cancel = new GameObject();
        cancel.name = "CancelBut";

        Button cancelBtn = cancel.AddComponent<Button>();
        int cancelId = 3;
        cancelBtn.onClick.AddListener(() => OnButtonClicked(cancelId));

        Image cancelImg = cancel.AddComponent<Image>();
        cancelImg.sprite = Resources.Load("images/nmxBigButtonBlue", typeof(Sprite)) as Sprite;

        GameObject textGO = new GameObject();
        textGO.transform.SetParent(cancel.transform);
        Text txt = textGO.gameObject.AddComponent<Text>();
        txt.text = "Close";
        txt.alignment = TextAnchor.MiddleCenter;
        txt.font = Resources.Load<Font>("Fonts/PlayRegular");
        txt.color = Color.white;
        txt.fontSize = 24;

        cancel.GetComponent<RectTransform>().sizeDelta = buttonSize;
        //AddComponent(cancel);
        AddRowElement(cancel);


        float x = (a2 != null) ? (buttonSize.x + butPadding) * 2 : buttonSize.x + butPadding;
        float y = 0f;
        cancel.GetComponent<RectTransform>().anchoredPosition = new Vector2(LineX, y);
    }


    private void OnButtonClicked(int id)
    {
        switch(id)
        {
            case 1:
                a1();
                DestroyObject();
                break;
            case 2:
                a2();
                DestroyObject();
                break;
            case 3:
                DestroyObject();
                break;
        }
    }

}
