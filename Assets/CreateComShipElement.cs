﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CreateComShipElement : CreateActionElement
{
    public CommanderShip comShip;
    private string planetId;
    public ActionViewController actionView;

    private UnityEvent trainCom;
    private UnityEvent buyCom;
    private UnityEvent upgCom;
    private UnityEvent buyUpgCom;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnEnable()
    {
        SetViewInfo();
        SetButtonsFunction();
    }


    private void SetViewInfo()
    {
        planetId = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID].id;
        actionView = GameManager.instance.CurrentView.GetComponent<ActionViewController>();
    }


    protected override void SetButtonsFunction()
    {
        Button train = this.gameObject.transform.Find("Train").Find("Train").GetComponent<Button>();
        Button upgrade = this.gameObject.transform.Find("Upgrade").Find("Upg").GetComponent<Button>();
        Button cancel = this.gameObject.transform.Find("Delete").Find("Cancel").GetComponent<Button>();

        train.onClick.AddListener(() => OnTrainClick());
        upgrade.onClick.AddListener(() => OnUpgClick());
        cancel.onClick.AddListener(() => OnCancelClick());
    }


    public override void SetElement()
    {
        SetParent();
        SetComShipInfo();
        SetBuildingImage();
    }


    private void SetComShipInfo()
    {
        var name = gameObject.transform.Find("Info").Find("Name");
        name.GetComponent<Text>().text = comShip.Name;

        var train = gameObject.transform.Find("Train").Find("Train");
        train.GetComponent<Button>().interactable = ((!comShip.IsBuilt || comShip.IsDead) && !comShip.InQueue);

        var upg = gameObject.transform.Find("Upgrade").Find("Upg");
        upg.GetComponent<Button>().interactable = (!comShip.UpgInProgress);

        var cancel = gameObject.transform.Find("Delete").Find("Cancel");
        if (comShip.Id == 2)
        {
            //Debug.Log(comShip.UpgInProgress);   
        }
        
        cancel.GetComponent<Button>().interactable = (comShip.InQueue || comShip.UpgInProgress);
    }


    protected override void SetBuildingImage()
    {
        StartCoroutine(NetworkManager.instance.loadImage(comShip.PicUrl, SetImage));
    }

    // ***TRAIN***
    private void OnTrainClick()
    {
        ActionViewPopUp trainPopup = new ActionViewPopUp();
        trainPopup.SetButtonsCallbacks(OnTrain, OnBuyTrain);
        if (comShip.IsBuilt && comShip.IsDead)
        {
            string desc = "Normal repair will last " + comShip.RepairTime + " and cost you " + comShip.RepairMetal + " Metal, " + comShip.RepairCrystal + " Crystal and " +
                comShip.RepairGas + " Gas. Or choose instant repair for " + comShip.UpgCredits + " Credits!";
            trainPopup.DescriptionText = desc;
            trainPopup.SetButtonsNames("Repair", "Instant");
        }
        else if (!comShip.IsBuilt)
        {
            string desc = "Building will last " + comShip.BuildTime + " and cost you " + comShip.BuildMetal + " Metal, " + comShip.BuildCrystal + " Crystal and " +
                comShip.BuildGas + " Gas. Or you can instant buy it for " + comShip.UpgCredits + " Credits!";
            trainPopup.DescriptionText = desc;
            trainPopup.SetButtonsNames("Train", "Buy");
        }
        
        trainPopup.Create();
    }

    private void OnTrain()
    {
        NetworkManager.instance.TrainCommanderShip(PlayerManager.instance.SessionToken, comShip.Id, planetId, actionView.OnComShipTrain);
    }

    private void OnBuyTrain()
    {
        NetworkManager.instance.BuyCommanderShip(PlayerManager.instance.SessionToken, comShip.Id, planetId, actionView.OnComShipTrain);
    }


    // ***UPGRADE***
    protected override void OnUpgClick()
    {
        ActionViewPopUp upgPopup = new ActionViewPopUp();
        upgPopup.SetButtonsCallbacks(OnUp, OnUpBuy);
        string desc = "Normal upgrade will last " + comShip.UpgTime + " seconds and cost you " + comShip.UpgMetal + " Metal, " + comShip.UpgCrystal + " Crystal and " +
                comShip.UpgGas + " Gas. Or choose buy instant upgrade for " + comShip.UpgCredits + " Credits!";
        upgPopup.DescriptionText = desc;
        upgPopup.SetButtonsNames("Upgrade", "Instant");
        upgPopup.Create();
    }

    private void OnUp()
    {
        NetworkManager.instance.StartCommanderShipUpgrade(PlayerManager.instance.SessionToken, comShip.Id, planetId, actionView.OnComShipUpgrade);
    }

    private void OnUpBuy()
    {
        NetworkManager.instance.BuyCommanderShipUpgrade(PlayerManager.instance.SessionToken, comShip.Id, planetId, actionView.OnComShipUpgrade);
    }


    protected override void OnCancelClick()
    {
        base.OnCancelClick();
    }

}
