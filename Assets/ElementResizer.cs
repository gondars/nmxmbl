﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ElementResizer : MonoBehaviour
{
    private Image img;
    public Sprite sprite;
    public float height;
    public Sprite[] backgrounds;

    void Start()
    {
        Debug.Log("da");
        img = gameObject.transform.GetComponent<Image>();
        SetImage(sprite);
        SetSize();

    }

    public void SetImage(Sprite spr)
    {
        img.sprite = spr;
    }

    public Sprite GetImage()
    {
        return img.sprite;
    }

    public float GetAspectRatio()
    {
        return img.sprite.rect.width / img.sprite.rect.height;
    }


    public void SetSize()
    {
        img.rectTransform.sizeDelta = new Vector2(GetAspectRatio() * height, height);
        Debug.Log(Screen.height + " " + img.rectTransform.sizeDelta.y);
    }


    public void ChangeBg(int index)
    {
        if (GetImage() == backgrounds[index])
        {
            return;
        }
        Debug.Log("setting new image");
        SetImage(backgrounds[index]);
        SetSize();
    }
}
