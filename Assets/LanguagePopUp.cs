﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class LanguagePopUp : DynamicPopUp
{
    private int elementsPerRow = 4;
    private Vector2 elementSize = new Vector2(Screen.width * .2f, Screen.width * .2f);
    private float padding = Screen.width * .01f;
    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {

    }
    // Update is called once per frame

    public override void Create()
    {
        base.Create();
        base.SetName("Language Pop-Up");

        AddLanguages();
        GetBounds(mainContainer);
        SetBackground();
        FinishDrawing();
    }

    void AddLanguages()
    {
        int languageCounter = 0;

        foreach (var str in GameManager.instance.universes)
        {
            

            GameObject go = new GameObject();
            go.name = str.Key;

            Button btn = go.AddComponent<Button>();
            Image img = go.AddComponent<Image>();
            img.color = new Color32(175, 175, 175, 255);

            GameObject textGO = new GameObject();
            textGO.transform.SetParent(go.transform);
            Text txt = textGO.gameObject.AddComponent<Text>();
            txt.text = str.Key;
            txt.alignment = TextAnchor.MiddleCenter;
            txt.font = Resources.Load<Font>("Fonts/PlayRegular");
            txt.color = Color.black;
            txt.fontSize = 24;
            btn.onClick.AddListener(() => selectLanguage(txt.text));
            

            go.GetComponent<RectTransform>().sizeDelta = elementSize;
            AddComponent(go);
            double prm = Math.Floor(Convert.ToDouble(languageCounter / elementsPerRow));
            Debug.Log("Counter " + languageCounter + " PRM " + prm);
            go.GetComponent<RectTransform>().anchoredPosition = new Vector2((languageCounter % elementsPerRow) * (elementSize.x + padding), -(float)prm * (elementSize.y + padding));
            languageCounter++;
           
        }

    }


    public void selectLanguage(string lang = "sk")
    {
        NetworkManager.instance.gameId = lang.ToLower();
        base.DestroyObject();
    }
}
