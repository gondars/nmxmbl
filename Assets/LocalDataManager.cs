﻿using UnityEngine;
using System.Collections;

public class LocalDataManager : MonoBehaviour
{
    public static LocalDataManager instance = null;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SaveData(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    public string LoadData(string key)
    {
        return PlayerPrefs.GetString(key);
    }
}
