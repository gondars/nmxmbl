﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;

public class Defence
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }

    public bool CanConstruct { get; set; }
    public string PicUrl { get; set; }

    public string QueueId { get; set; }

    public int Available { get; set; }


    public Defence(JsonData response)
    {
        Id = Int32.Parse(response["id"].ToString());
        Name = response["name"].ToString();
        if (response["available"] == null)
        {
            Available = 0;
        }
        else
        {
            Available = Int32.Parse(response["available"].ToString());
        }

        //Type = "Upgrade";
        switch (Id)
        {
            case 1:
            case 2:
            case 10:
            case 11:
            case 12:
            case 13:
                Type = "CivilianShips";
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                Type = "WarShips";
                break;
            default:
                break;
        }


        if (response["can_be_constructed"].ToString() == "0")
        {
            CanConstruct = false;
        }
        else if (response["can_be_constructed"].ToString() == "1")
        {
            CanConstruct = true;
        }
        else
        {
            CanConstruct = (bool)response["can_be_constructed"];
        }

        PicUrl = "http://static.nemexia.net/game_dev/img/v2.0/img/defence/race1/thumbs/defence_" + Id + ".png";
    }

    public void SetQueueId(string id)
    {
        QueueId = id;
    }
}