﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using LitJson;
using UnityEngine.Events;
using System.Collections.Generic;

public class CreateActionElement : MonoBehaviour
{
    private string prefabType;
    public int id;
    public Building building;
    public Upgrade upgrade;
    public Science science;
    public Ship ship;
    public Defence defence;

    private UnityEvent speedUpEvent;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnEnable()
    {
        speedUpEvent = new UnityEvent();
        prefabType = GameManager.instance.actionType;

        if (prefabType == C_ActionType.BUILDINGS)
        {
            //SetBuilding();
            //Debug.Log(prefabType);
        }

        InvokeRepeating("SetElement", 0f, 10.00f);

        SetButtonsFunction();
    }

    void OnDisable()
    {
        CancelInvoke("SetElement");
    }

    virtual public void SetElement()
    {
        if (prefabType == C_ActionType.BUILDINGS)
        {
            for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
            {
                if (id == PlayerManager.instance.buildings[i].Id)
                {
                    building = PlayerManager.instance.buildings[i];
                }
            }
            SetParent();
            SetBuildingInfo();
            SetBuildingImage();
        }
        else if (prefabType == C_ActionType.UPGRADES)
        {
            for (int i = 0; i < PlayerManager.instance.upgrades.Length; i++)
            {
                if (id == PlayerManager.instance.upgrades[i].Id)
                {
                    upgrade = PlayerManager.instance.upgrades[i];
                }
            }
            SetParent();
            SetUpgradesInfo();
            SetBuildingImage();
        }
        else if (prefabType == C_ActionType.SCIENCES)
        {
            for (int i = 0; i < PlayerManager.instance.sciences.Length; i++)
            {
                if (id == PlayerManager.instance.sciences[i].Id)
                {
                    science = PlayerManager.instance.sciences[i];
                }
            }
            SetParent();
            SetSciencesInfo();
            SetBuildingImage();
        }
        else if (prefabType == C_ActionType.SHIPS)
        {
            for (int i = 0; i < PlayerManager.instance.ships.Length; i++)
            {
                if (id == PlayerManager.instance.ships[i].Id)
                {
                    ship = PlayerManager.instance.ships[i];
                }
            }
            SetParent();
            SetShipsInfo();
            SetBuildingImage();
        }
        else if (prefabType == C_ActionType.DEFENCES)
        {
            for (int i = 0; i < PlayerManager.instance.defences.Length; i++)
            {
                if (id == PlayerManager.instance.defences[i].Id)
                {
                    defence = PlayerManager.instance.defences[i];
                }
            }
            SetParent();
            SetDefencesInfo();
            SetBuildingImage();
        }
    }

    protected void SetParent()
    {
        if (GameManager.instance.CurrentView == GameManager.instance.actionView)
        {
            //if (prefabType == C_ActionType.BUILDINGS)
            //{
            //    this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag(building.Type).transform, false);
            //}
            //else if (prefabType == C_ActionType.UPGRADES)
            //{
            //    this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag(upgrade.Type).transform, false);
            //}
            //else if (prefabType == C_ActionType.SCIENCES)
            //{
            //    this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag(science.Type).transform, false);
            //}
            //else if(prefabType == C_ActionType.SHIPS)
            //{
            //    this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag(ship.Type).transform, false);
            //}
            //else if (prefabType == C_ActionType.DEFENCES)
            //{
            //    this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag(defence.Type).transform, false);
            //}
            
            if (gameObject.transform.parent != GameObject.FindGameObjectWithTag("ScrollContainer").transform)
            {
                this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag("ScrollContainer").transform, false);
            }
        }
    }

    void SetBuildingInfo()
    {
        var name = this.gameObject.transform.Find("Info").Find("Name");
        name.GetComponent<Text>().text = building.Name;

        var level = this.gameObject.transform.Find("Info").Find("Level");
        level.GetComponent<LocalizedText>().specialString = building.Level.ToString();
        level.GetComponent<LocalizedText>().LocalizedID = "Level";


        var upgIcon = this.gameObject.transform.Find("Upgrade");
        var deleteIcon = this.gameObject.transform.Find("Delete");

        if (building.InProgress)
        {
            upgIcon.Find("Upg").gameObject.SetActive(false);
            upgIcon.Find("SpeedUp").gameObject.SetActive(true);

            deleteIcon.Find("Demolish").gameObject.SetActive(false);
            deleteIcon.Find("Cancel").gameObject.SetActive(true);
        }
        else
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);

            deleteIcon.Find("Demolish").gameObject.SetActive(true);
            deleteIcon.Find("Cancel").gameObject.SetActive(false);

            deleteIcon.Find("Demolish").GetComponent<Button>().interactable = building.CanBeDemolished;
            upgIcon.Find("Upg").GetComponent<Button>().interactable = building.CanUpgrade;

            if (!building.CanUpgrade)
            {
                //Debug.Log("[" + building.Name + "] " + building.CantUpgradeReason);
            }
        }
    }

    void SetUpgradesInfo()
    {
        var name = this.gameObject.transform.Find("Info").Find("Name");
        name.GetComponent<Text>().text = upgrade.Name;

        var level = this.gameObject.transform.Find("Info").Find("Level");
        level.GetComponent<LocalizedText>().specialString = upgrade.Level.ToString();
        level.GetComponent<LocalizedText>().LocalizedID = "Level";


        var upgIcon = this.gameObject.transform.Find("Upgrade");
        var deleteIcon = this.gameObject.transform.Find("Delete");

        Debug.Log(upgrade.InProgress);

        if (upgrade.InProgress)
        {
            upgIcon.Find("Upg").gameObject.SetActive(false);
            upgIcon.Find("SpeedUp").gameObject.SetActive(true);

            deleteIcon.Find("Demolish").gameObject.SetActive(false);
            deleteIcon.Find("Cancel").gameObject.SetActive(true);
        }
        else
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);

            deleteIcon.Find("Demolish").gameObject.SetActive(true);
            deleteIcon.Find("Cancel").gameObject.SetActive(false);

            deleteIcon.Find("Demolish").GetComponent<Button>().interactable = false; // only buildings
            upgIcon.Find("Upg").GetComponent<Button>().interactable = upgrade.CanUpgrade;
        }
    }

    void SetSciencesInfo()
    {
        var name = this.gameObject.transform.Find("Info").Find("Name");
        name.GetComponent<Text>().text = science.Name;

        var level = this.gameObject.transform.Find("Info").Find("Level");
        level.GetComponent<LocalizedText>().specialString = science.Level.ToString();
        level.GetComponent<LocalizedText>().LocalizedID = "Level";


        var upgIcon = this.gameObject.transform.Find("Upgrade");
        var deleteIcon = this.gameObject.transform.Find("Delete");

        if (science.InProgress)
        {
            upgIcon.Find("Upg").gameObject.SetActive(false);
            upgIcon.Find("SpeedUp").gameObject.SetActive(true);

            deleteIcon.Find("Demolish").gameObject.SetActive(false);
            deleteIcon.Find("Cancel").gameObject.SetActive(true);
        }
        else
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);

            deleteIcon.Find("Demolish").gameObject.SetActive(true);
            deleteIcon.Find("Cancel").gameObject.SetActive(false);

            deleteIcon.Find("Demolish").GetComponent<Button>().interactable = false; // we cant demolish sciences, only buildings
            upgIcon.Find("Upg").GetComponent<Button>().interactable = science.CanUpgrade;
        }
    }

    void SetShipsInfo()
    {
        var name = this.gameObject.transform.Find("Info").Find("Name");
        name.GetComponent<Text>().text = ship.Name;

        var level = this.gameObject.transform.Find("Info").Find("Level");
        level.GetComponent<LocalizedText>().specialString = ship.Available.ToString();

        List<string> shipsMessages = new List<string>();
        shipsMessages.Add("nmx_AvailableShips");
        shipsMessages.Add(" " + ship.Available);

        level.GetComponent<LocalizedText>().LocalizedIDs = shipsMessages;


        var upgIcon = this.gameObject.transform.Find("Upgrade");
        var deleteIcon = this.gameObject.transform.Find("Delete");

        deleteIcon.Find("Demolish").gameObject.SetActive(false);
        deleteIcon.Find("Cancel").gameObject.SetActive(true);

        //Debug.Log("[SetShipsInfo] " + "queueId : " + ship.QueueId);
        if (ship.CanConstruct && ship.QueueId == null)
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);
            deleteIcon.Find("Cancel").GetComponent<Button>().interactable = false;
            upgIcon.Find("Upg").GetComponent<Button>().interactable = true;

        }
        else if ((ship.CanConstruct && ship.QueueId != null) || (!ship.CanConstruct && ship.QueueId != null))
        {
            upgIcon.Find("Upg").gameObject.SetActive(false);
            upgIcon.Find("SpeedUp").gameObject.SetActive(true);
            deleteIcon.Find("Cancel").GetComponent<Button>().interactable = true;

        }
        else if (!ship.CanConstruct && ship.QueueId == null)
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);
            upgIcon.Find("Upg").GetComponent<Button>().interactable = false;
            deleteIcon.Find("Cancel").GetComponent<Button>().interactable = false;
        }
    }

    void SetDefencesInfo()
    {
        var name = this.gameObject.transform.Find("Info").Find("Name");
        name.GetComponent<Text>().text = defence.Name;

        var level = this.gameObject.transform.Find("Info").Find("Level");
        level.GetComponent<LocalizedText>().specialString = defence.Available.ToString();

        List<string> defencesMessages = new List<string>();
        defencesMessages.Add("nmx_AvailableShips");
        defencesMessages.Add(" " + defence.Available);

        level.GetComponent<LocalizedText>().LocalizedIDs = defencesMessages;


        var upgIcon = this.gameObject.transform.Find("Upgrade");
        var deleteIcon = this.gameObject.transform.Find("Delete");

        deleteIcon.Find("Demolish").gameObject.SetActive(false);
        deleteIcon.Find("Cancel").gameObject.SetActive(true);

        if (defence.CanConstruct && defence.QueueId == null)
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);
            deleteIcon.Find("Cancel").GetComponent<Button>().interactable = false;
            upgIcon.Find("Upg").GetComponent<Button>().interactable = true;

        }
        else if ((defence.CanConstruct && defence.QueueId != null) || (!defence.CanConstruct && defence.QueueId != null))
        {
            upgIcon.Find("Upg").gameObject.SetActive(false);
            upgIcon.Find("SpeedUp").gameObject.SetActive(true);
            deleteIcon.Find("Cancel").GetComponent<Button>().interactable = true;

        }
        else if (!defence.CanConstruct && defence.QueueId == null)
        {
            upgIcon.Find("Upg").gameObject.SetActive(true);
            upgIcon.Find("SpeedUp").gameObject.SetActive(false);
            upgIcon.Find("Upg").GetComponent<Button>().interactable = false;
            deleteIcon.Find("Cancel").GetComponent<Button>().interactable = false;
        }
    }

    virtual protected void SetBuildingImage()
    {
        if (prefabType == C_ActionType.BUILDINGS)
        {
            StartCoroutine(NetworkManager.instance.loadImage(building.PicUrl, SetImage));
        }
        else if (prefabType == C_ActionType.UPGRADES)
        {
            StartCoroutine(NetworkManager.instance.loadImage(upgrade.PicUrl, SetImage));
        }
        else if (prefabType == C_ActionType.SCIENCES)
        {
            StartCoroutine(NetworkManager.instance.loadImage(science.PicUrl, SetImage));
        }
        else if (prefabType == C_ActionType.SHIPS)
        {
            StartCoroutine(NetworkManager.instance.loadImage(ship.PicUrl, SetImage));
        }
        else if (prefabType == C_ActionType.DEFENCES)
        {
            StartCoroutine(NetworkManager.instance.loadImage(defence.PicUrl, SetImage));
        }
    }

    protected void SetImage(Texture2D response)
    {
        Rect rec = new Rect(0, 0, response.width, response.height);

        this.gameObject.transform.Find("ImagePanel").Find("Image").GetComponent<Image>().sprite = Sprite.Create(response, rec, new Vector2(0, 0));
    }

    virtual protected void SetButtonsFunction()
    {
        Button upg = this.gameObject.transform.Find("Upgrade").Find("Upg").GetComponent<Button>();
        Button speedUp = this.gameObject.transform.Find("Upgrade").Find("SpeedUp").GetComponent<Button>();
        Button cancel = this.gameObject.transform.Find("Delete").Find("Cancel").GetComponent<Button>();
        Button demolish = this.gameObject.transform.Find("Delete").Find("Demolish").GetComponent<Button>();

        upg.onClick.AddListener(() => OnUpgClick());
        speedUp.onClick.AddListener(() => OnSpeedUpClick());
        cancel.onClick.AddListener(() => OnCancelClick());
        demolish.onClick.AddListener(() => OnDemolishClick());

    }

    virtual protected void OnUpgClick()
    {
        string planetId = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID].id;

        if (prefabType == C_ActionType.BUILDINGS)
        {


            // for test - uncomment this blcok and comment the request
            //for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
            //{
            //    if (id == PlayerManager.instance.buildings[i].Id)
            //    {
            //        PlayerManager.instance.buildings[i].InProgress = true;
            //        PlayerManager.instance.buildings[i].CanUpgrade = false;

            //        SetElement();
            //    }
            //}

            NetworkManager.instance.ConstructBuilding(PlayerManager.instance.SessionToken, "sk", "1", planetId, building.Id.ToString(), OnUpgCallback);
        }
        else if (prefabType == C_ActionType.SCIENCES)
        {
            NetworkManager.instance.UpgradeSciences(PlayerManager.instance.SessionToken, "sk", "1", (science.Id).ToString(), planetId, OnScienceUpgradeResponse);
        }
        else if (prefabType == C_ActionType.UPGRADES)
        {
            NetworkManager.instance.UpgradeShip(PlayerManager.instance.SessionToken, "sk", "1", (upgrade.Id).ToString(), planetId, OnShipUpgradeResponse);
        }
        else if (prefabType == C_ActionType.SHIPS)
        {
            NetworkManager.instance.TrainShip(PlayerManager.instance.SessionToken, "sk", "1", (ship.Id).ToString(), planetId, OnShipTrainResponse);
        }
        else if (prefabType == C_ActionType.DEFENCES)
        {
            NetworkManager.instance.TrainDefence(PlayerManager.instance.SessionToken, "sk", "1", (defence.Id).ToString(), planetId, OnDefenceTrainResponse);
        }
    }

    private void OnUpgCallback(JsonData response)
    {
        IDictionary tdictionary = response as IDictionary;
        if (tdictionary.Contains("success"))
        {
            Debug.Log("Upg Success");

            if ((bool)response["success"] == true)
            {
                for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
                {
                    if (id == PlayerManager.instance.buildings[i].Id)
                    {
                        PlayerManager.instance.buildings[i].InProgress = true;
                        PlayerManager.instance.buildings[i].CanUpgrade = false;


                        JsonData elementInfo = response["process"];
                        PlayerManager.instance.buildings[i].QueueId = elementInfo["id"].ToString();
                        PlayerManager.instance.buildings[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
        }
        else
        {
            Debug.Log("Upg Fail");
        }

    }

    virtual protected void OnSpeedUpClick()
    {
        if (prefabType == C_ActionType.BUILDINGS)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", building.QueueId, "buildings", "0", SpeedUpConfirmResponse);
        }
        if (prefabType == C_ActionType.UPGRADES)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", upgrade.QueueId, "shipsupgrades", "0", SpeedUpConfirmResponse);
        }
        if (prefabType == C_ActionType.SCIENCES)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", science.QueueId, "sciences", "0", SpeedUpConfirmResponse);
        }
        if (prefabType == C_ActionType.SHIPS)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", ship.QueueId, "ships", "0", SpeedUpConfirmResponse);
        }
        if (prefabType == C_ActionType.DEFENCES)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", defence.QueueId, "defence", "0", SpeedUpConfirmResponse);
        }
    }

    private void SpeedUpConfirmResponse(JsonData response)
    {
        speedUpEvent.RemoveListener(RequestSpeedUp);
        speedUpEvent.AddListener(RequestSpeedUp);
        GameManager.instance.EnableDynamicView(response["message"].ToString(), "speedUp", speedUpEvent);
    }

    private void RequestSpeedUp()
    {
        if (prefabType == C_ActionType.BUILDINGS)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", building.QueueId, "buildings", "1", SpeedUpResponse);
        }
        if (prefabType == C_ActionType.UPGRADES)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", upgrade.QueueId, "shipsupgrades", "1", SpeedUpResponse);
        }
        if (prefabType == C_ActionType.SCIENCES)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", science.QueueId, "sciences", "1", SpeedUpResponse);
        }
        if (prefabType == C_ActionType.SHIPS)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", ship.QueueId, "ships", "1", SpeedUpResponse);
        }
        if (prefabType == C_ActionType.DEFENCES)
        {
            NetworkManager.instance.SpeedUp(PlayerManager.instance.SessionToken, "sk", "1", defence.QueueId, "defence", "1", SpeedUpResponse);
        }
    }

    private void SpeedUpResponse(JsonData response)
    {
        if ((bool)response["success"] == false)
        {
            Debug.Log("Something went wrong with speed up building");
            return;
        }
        else
        {
            if (prefabType == C_ActionType.BUILDINGS)
            {
                for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
                {
                    if (id == PlayerManager.instance.buildings[i].Id)
                    {
                        //PlayerManager.instance.buildings[i].InProgress = false;
                        //PlayerManager.instance.buildings[i].CanUpgrade = true;

                        //JsonData elementInfo = response["process"];
                        //PlayerManager.instance.buildings[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
            else if (prefabType == C_ActionType.UPGRADES)
            {
                for (int i = 0; i < PlayerManager.instance.upgrades.Length; i++)
                {
                    if (id == PlayerManager.instance.upgrades[i].Id)
                    {
                        //PlayerManager.instance.buildings[i].InProgress = false;
                        //PlayerManager.instance.buildings[i].CanUpgrade = true;

                        //JsonData elementInfo = response["process"];
                        //PlayerManager.instance.buildings[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
            else if (prefabType == C_ActionType.SCIENCES)
            {
                for (int i = 0; i < PlayerManager.instance.sciences.Length; i++)
                {
                    if (id == PlayerManager.instance.sciences[i].Id)
                    {
                        //PlayerManager.instance.buildings[i].InProgress = false;
                        //PlayerManager.instance.buildings[i].CanUpgrade = true;

                        //JsonData elementInfo = response["process"];
                        //PlayerManager.instance.buildings[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
            else if (prefabType == C_ActionType.SHIPS)
            {
                for (int i = 0; i < PlayerManager.instance.ships.Length; i++)
                {
                    if (id == PlayerManager.instance.ships[i].Id)
                    {
                        //PlayerManager.instance.buildings[i].InProgress = false;
                        //PlayerManager.instance.buildings[i].CanUpgrade = true;

                        //JsonData elementInfo = response["process"];
                        //PlayerManager.instance.buildings[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
            else if (prefabType == C_ActionType.DEFENCES)
            {
                for (int i = 0; i < PlayerManager.instance.defences.Length; i++)
                {
                    if (id == PlayerManager.instance.defences[i].Id)
                    {
                        //PlayerManager.instance.buildings[i].InProgress = false;
                        //PlayerManager.instance.buildings[i].CanUpgrade = true;

                        //JsonData elementInfo = response["process"];
                        //PlayerManager.instance.buildings[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }

            GameManager.instance.EnableDynamicView(response["message"].ToString(), "");
        }
    }

    virtual protected void OnCancelClick()
    {
        if (prefabType == C_ActionType.BUILDINGS)
        {
            NetworkManager.instance.CancelBuildingUpgrade(PlayerManager.instance.SessionToken, "sk", "1", building.QueueId, OnCancelResponse);
        }
        else if (prefabType == C_ActionType.SCIENCES)
        {
            NetworkManager.instance.CancelSciences(PlayerManager.instance.SessionToken, "sk", "1", science.QueueId, OnCancelResponse);
        }
        else if (prefabType == C_ActionType.UPGRADES)
        {
            NetworkManager.instance.CancelUpgrades(PlayerManager.instance.SessionToken, "sk", "1", upgrade.QueueId, OnCancelResponse);
        }
        else if (prefabType == C_ActionType.SHIPS)
        {
            NetworkManager.instance.CancelShipTraining(PlayerManager.instance.SessionToken, "sk", "1", ship.QueueId, OnCancelResponse);
        }
        else if (prefabType == C_ActionType.DEFENCES)
        {
            NetworkManager.instance.CancelDefenceTrain(PlayerManager.instance.SessionToken, "sk", "1", defence.QueueId, OnCancelResponse);
        }
    }

    private void OnCancelResponse(JsonData response)
    {
        if ((bool)response["success"] == false)
        {
            Debug.Log("Something went wrong with cancel bulding upgrade");
            return;
        }
        else
        {


            if (prefabType == C_ActionType.BUILDINGS)
            {
                JsonData resources = response["restoredResources"] as JsonData;

                for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
                {
                    if (id == PlayerManager.instance.buildings[i].Id)
                    {
                        PlayerManager.instance.buildings[i].InProgress = false;
                        PlayerManager.instance.buildings[i].CanUpgrade = true;
                        PlayerManager.instance.buildings[i].QueueId = null;
                        PlayerManager.instance.buildings[i].Level--;
                        if (PlayerManager.instance.buildings[i].Level != 0)
                        {
                            PlayerManager.instance.buildings[i].CanBeDemolished = true;
                        }
                        else
                        {
                            PlayerManager.instance.buildings[i].CanBeDemolished = false;
                        }

                        SetElement();
                    }
                }
            }

            else if (prefabType == C_ActionType.SCIENCES)
            {
                JsonData resources = response["returnedResources"] as JsonData;

                for (int i = 0; i < PlayerManager.instance.sciences.Length; i++)
                {
                    if (id == PlayerManager.instance.sciences[i].Id)
                    {
                        PlayerManager.instance.sciences[i].InProgress = false;
                        PlayerManager.instance.sciences[i].CanUpgrade = true;
                        PlayerManager.instance.sciences[i].QueueId = null;
                        SetElement();
                    }
                }
            }

            else if (prefabType == C_ActionType.UPGRADES)
            {
                JsonData resources = response["returnedResources"] as JsonData;

                for (int i = 0; i < PlayerManager.instance.upgrades.Length; i++)
                {
                    if (id == PlayerManager.instance.upgrades[i].Id)
                    {
                        PlayerManager.instance.upgrades[i].InProgress = false;
                        PlayerManager.instance.upgrades[i].CanUpgrade = true;
                        PlayerManager.instance.upgrades[i].QueueId = null;
                        SetElement();
                    }
                }
            }

            else if (prefabType == C_ActionType.SHIPS)
            {
                JsonData resources = response["restoredResources"] as JsonData;

                for (int i = 0; i < PlayerManager.instance.ships.Length; i++)
                {
                    if (id == PlayerManager.instance.ships[i].Id)
                    {
                        PlayerManager.instance.ships[i].QueueId = null;
                        PlayerManager.instance.ships[i].CanConstruct = true;
                        SetElement();
                    }
                }
            }

            else if (prefabType == C_ActionType.DEFENCES)
            {
                JsonData resources = response["restoredResources"] as JsonData;

                for (int i = 0; i < PlayerManager.instance.defences.Length; i++)
                {
                    if (id == PlayerManager.instance.defences[i].Id)
                    {
                        PlayerManager.instance.defences[i].QueueId = null;
                        PlayerManager.instance.defences[i].CanConstruct = true;
                        SetElement();
                    }
                }
            }
        }
    }

    private void OnDemolishClick()
    {
        string planetId = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID].id;

        NetworkManager.instance.DemolishBuilding(PlayerManager.instance.SessionToken, "sk", "1", (building.Id).ToString(), planetId, OnDemolishResponse);

    }

    private void OnDemolishResponse(JsonData response)
    {

        IDictionary tdictionary = response as IDictionary;
        if (tdictionary.Contains("error"))
        {
            Debug.Log("Something went wrong with demolishing building");
            return;
        }
        else
        {
            for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
            {
                if (id == PlayerManager.instance.buildings[i].Id)
                {
                    PlayerManager.instance.buildings[i].Level--;
                    if (PlayerManager.instance.buildings[i].Level != 0)
                    {
                        PlayerManager.instance.buildings[i].CanBeDemolished = true;
                    }
                    else
                    {
                        PlayerManager.instance.buildings[i].CanBeDemolished = false;
                    }
                    SetElement();
                }
            }
        }
    }

    //sciences
    private void OnScienceUpgradeResponse(JsonData response)
    {
        IDictionary tdictionary = response as IDictionary;
        if (tdictionary.Contains("success"))
        {
            Debug.Log("Upg Success");

            if ((bool)response["success"] == true)
            {
                for (int i = 0; i < PlayerManager.instance.sciences.Length; i++)
                {
                    if (id == PlayerManager.instance.sciences[i].Id)
                    {
                        PlayerManager.instance.sciences[i].InProgress = true;
                        PlayerManager.instance.sciences[i].CanUpgrade = false;

                        JsonData elementInfo = response["process"];
                        PlayerManager.instance.sciences[i].QueueId = elementInfo["id"].ToString();
                        PlayerManager.instance.sciences[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
        }
        else
        {
            Debug.Log("Upg Fail");
        }
    }

    private void OnShipUpgradeResponse(JsonData response)
    {
        IDictionary tdictionary = response as IDictionary;
        if (tdictionary.Contains("success"))
        {
            Debug.Log("Upg Success");

            if ((bool)response["success"] == true)
            {
                for (int i = 0; i < PlayerManager.instance.upgrades.Length; i++)
                {
                    if (id == PlayerManager.instance.upgrades[i].Id)
                    {
                        PlayerManager.instance.upgrades[i].InProgress = true;
                        PlayerManager.instance.upgrades[i].CanUpgrade = false;

                        JsonData elementInfo = response["process"];
                        PlayerManager.instance.upgrades[i].QueueId = elementInfo["id"].ToString();
                        PlayerManager.instance.upgrades[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
        }
        else
        {
            Debug.Log("Upg Fail");
        }
    }

    private void OnShipTrainResponse(JsonData response)
    {
        IDictionary tdictionary = response as IDictionary;
        if (tdictionary.Contains("success"))
        {
            Debug.Log("Upg Success");

            if ((bool)response["success"] == true)
            {
                for (int i = 0; i < PlayerManager.instance.ships.Length; i++)
                {
                    if (id == PlayerManager.instance.ships[i].Id)
                    {
                        //PlayerManager.instance.ships[i].InProgress = true;
                        //PlayerManager.instance.upgrades[i].CanUpgrade = false;


                        JsonData elementInfo = response["process"];
                        PlayerManager.instance.ships[i].QueueId = elementInfo["id"].ToString();
                        //PlayerManager.instance.ships[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
        }
        else
        {
            Debug.Log("Upg Fail");
        }
    }

    private void OnDefenceTrainResponse(JsonData response)
    {
        IDictionary tdictionary = response as IDictionary;
        if (tdictionary.Contains("success"))
        {
            Debug.Log("Upg Success");

            if ((bool)response["success"] == true)
            {
                for (int i = 0; i < PlayerManager.instance.defences.Length; i++)
                {
                    if (id == PlayerManager.instance.defences[i].Id)
                    {
                        //PlayerManager.instance.defences[i].InProgress = true;
                        //PlayerManager.instance.defences[i].CanUpgrade = false;

                        JsonData elementInfo = response["process"];
                        PlayerManager.instance.defences[i].QueueId = elementInfo["id"].ToString();
                        //PlayerManager.instance.defences[i].Level = Int32.Parse(elementInfo["level"].ToString());
                        SetElement();
                    }
                }
            }
        }
        else
        {
            Debug.Log("Upg Fail");
        }
    }
}
