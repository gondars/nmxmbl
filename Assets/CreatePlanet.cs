﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreatePlanet : MonoBehaviour
{
    int i;
    public Sprite bg, teamBg;

    // Use this for initialization
    void Start()
    {

        this.gameObject.transform.SetParent(GameObject.FindGameObjectWithTag(Constants.PLANET_CONTAINER).transform);
        this.gameObject.transform.localScale = new Vector3(1, 1, 1);

        i = transform.GetSiblingIndex();

        DynamicPlanetInfo();
        StaticPlanetInfo();
    }

    // Update is called once per frame
    void Update()
    {
        //DynamicPlanetInfo();
    }

    void OnEnable()
    {
        InvokeRepeating("RefreshInfo", 0.0f, 5.00f);
    }

    void OnDisable()
    {
        CancelInvoke("RefreshInfo");
    }

    private void RefreshInfo()
    {
        DynamicPlanetInfo();
    }

    private void DynamicPlanetInfo()
    {
        var planetInfo = this.gameObject.transform.Find("PlanetInfoHolder").Find("PlanetInfo").Find("Panel").Find("Text");
        planetInfo.GetComponent<Text>().text = PlayerManager.instance.planets[i].name + "\n(" + PlayerManager.instance.planets[i].coordinates.x + ":" +
            PlayerManager.instance.planets[i].coordinates.y + ":" + PlayerManager.instance.planets[i].coordinates.z + ")";

        var storage = this.gameObject.transform.Find("PlanetInfoHolder").Find("PlanetInfo").Find("StorageHolder");
        storage.GetChild(1).GetComponent<Text>().text = PlayerManager.instance.planets[i].storage + "%";

        if (PlayerManager.instance.planets[i].storage >= 100)
        {
            storage.GetChild(1).GetComponent<Text>().color = new Color(171f / 255f, 160f / 255f, 0);
        }
        else
        {
            storage.GetChild(1).GetComponent<Text>().color = new Color(147f / 255f, 199f / 255f, 199f / 255f);
        }

        Transform nameBackground = this.gameObject.transform.Find("PlanetInfoHolder").Find("PlanetInfo").Find("Panel");
        nameBackground.GetComponent<Image>().sprite = (PlayerManager.instance.planets[i].IsTeam == 0) ? bg : teamBg;

        Transform ozoneIndicator = this.gameObject.transform.Find("PlanetInfoHolder").Find("WarningsInfo").Find("OzoneWarning");
        ozoneIndicator.gameObject.SetActive(PlayerManager.instance.planets[i].ozoneWarning);

        Transform attackIndicator = this.gameObject.transform.Find("PlanetInfoHolder").Find("WarningsInfo").Find("Attacks");
        attackIndicator.gameObject.SetActive(PlayerManager.instance.planets[i].isAttacked);

        Transform planetImage = this.gameObject.transform.Find("PlanetInfoHolder").Find("Planet");
        Texture planetContainer = planetImage.GetComponent<Image>().GetComponent<Texture>();
        //planetImage.GetComponent<Image>().sprite = ;
        StartCoroutine(NetworkManager.instance.getTexture(PlayerManager.instance.planets[i].pictureId, setPlanetImage));
    }

    private void StaticPlanetInfo()
    {

    }

    private void setPlanetImage(Texture2D response)
    {

        Rect rec = new Rect(0, 0, response.width, response.height);

        Transform planetImage = this.gameObject.transform.Find("PlanetInfoHolder").Find("Planet");
        Texture planetContainer = planetImage.GetComponent<Image>().GetComponent<Texture>();
        planetImage.GetComponent<Image>().sprite = Sprite.Create(response, rec, new Vector2(0, 0));

        //for
    }

    public void onClick()
    {
        GameManager.instance.chosenPlanetID = i;
        GameManager.instance.ChangeView(GameManager.instance.gameView, GameManager.instance.planetView);
    }
}
