﻿using UnityEngine;
using System.Collections;
using LitJson;

public class Attack
{
    public Attack attack { get; set; }
    public string dePlanetId { get; set; }
    public string forwardTime { get; set; }


    public Attack(JsonData attackInfo)
    {
        dePlanetId = attackInfo["de_pl_id"].ToString();
    }
}
