﻿using UnityEngine;
using System.Collections;
using LitJson;

public class Opponent
{
    string Name { get; set; }
    string[] Coordinates { get; set; }

    public Opponent(JsonData response)
    {
        Name = response["player_name"].ToString();
        Coordinates = new string[3];
        Coordinates[0] = response["c1"].ToString();
        Coordinates[1] = response["c2"].ToString();
        Coordinates[2] = response["c3"].ToString();

        Debug.Log(Coordinates[2]);
    }
}
