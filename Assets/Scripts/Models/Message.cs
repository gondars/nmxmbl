﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;

public class Message
{
    public Message message { get; set; }

    //public string id { get; set; }
    public int newM { get; set; }
    public int oldM { get; set; }

    //public Message(DictionaryEntry messageInfo)
    //   {
    //       this.id = messageInfo.Key.ToString();
    //       this.newM = Int32.Parse((messageInfo.Value as JsonData)["new"].ToString());
    //       this.oldM = Int32.Parse((messageInfo.Value as JsonData)["old"].ToString());
    //   }

    public Message(object messageInfo)
    {
        //this.id = messageInfo.Key.ToString();
        this.newM = Int32.Parse((messageInfo as JsonData)["new"].ToString());
        this.oldM = Int32.Parse((messageInfo as JsonData)["old"].ToString());

        //Debug.Log(Int32.Parse((messageInfo as JsonData)["old"].ToString()));
    }
}
