﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;

public class CommanderShip
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsBuilt { get; set; }
    public bool IsAvailable { get; set; }
    public bool IsDead { get; set; }
    public bool InQueue { get; set; }
    public bool CanBeStarted { get; set; }
    public string PicUrl { get; set; }

    // build
    public string BuildTime { get; set; }
    public string BuildCredits { get; set; }
    public string BuildGas { get; set; }
    public string BuildMetal { get; set; }
    public string BuildCrystal { get; set; }
    // repair
    public string RepairTime { get; set; }
    public string RepairCredits { get; set; }
    public string RepairGas { get; set; }
    public string RepairMetal { get; set; }
    public string RepairCrystal { get; set; }

    // upgrade
    public bool CanStartUpg { get; set; }
    public bool UpgInProgress { get; set; }
    public string UpgTime { get; set; }
    public string UpgCredits { get; set; }
    public string UpgMetal { get; set; }
    public string UpgCrystal { get; set; }
    public string UpgGas { get; set; }

    public int testId;

    public CommanderShip(JsonData response)
    {
        testId = 2;
        SetComShipInfo(response);
    }

    public void SetComShipInfo(JsonData response)
    {
        Id = Int32.Parse(response["id"].ToString());
        Name = response["name"].ToString();
        IsBuilt = (bool)response["is_built"];
        IsAvailable = (bool)response["is_available"];
        IsDead = (bool)response["is_dead"];
        InQueue = (bool)response["in_queue"];
        CanBeStarted = (bool)response["can_be_started"];

        // build
        BuildTime = response["build"]["time"].ToString();
        BuildCredits = response["build"]["credits"].ToString();
        BuildGas = response["build"]["gas"].ToString();
        BuildMetal = response["build"]["metal"].ToString();
        BuildCrystal = response["build"]["crystal"].ToString();

        // repair
        RepairTime = response["repair"]["time"].ToString();
        RepairCredits = response["repair"]["credits"].ToString();
        RepairGas = response["repair"]["gas"].ToString();
        RepairMetal = response["repair"]["metal"].ToString();
        RepairCrystal = response["repair"]["crystal"].ToString();

        PicUrl = "http://static.nemexia.net/game_dev/img/v2.0/img/commanderShips/thumbs/commanderShip_" + Id + ".png";
        if (Id == 2)
        {
            //Debug.Log(response["upgrade_cost"]["time"].ToString());
            //Debug.Log(testId);
            Debug.Log(UpgInProgress);
            Debug.Log(UpgTime);
        }
    }

    public void SetUpgInfo(JsonData response)
    {
        CanStartUpg = (bool)response["can_be_started"];
        if (Id == 2)
        {
            //Debug.Log(response["upgrade_cost"]["time"].ToString());
            //Debug.Log(UpgInProgress);
        }
        UpgInProgress = (bool)response["in_progress"];
        
        UpgTime = response["upgrade_cost"]["time"].ToString();
        UpgCredits = response["upgrade_cost"]["credits"].ToString();
        UpgMetal = response["upgrade_cost"]["resources"]["metal"].ToString();
        UpgCrystal = response["upgrade_cost"]["resources"]["crystal"].ToString();
        UpgGas = response["upgrade_cost"]["resources"]["gas"].ToString();
    }
}
