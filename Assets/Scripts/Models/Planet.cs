﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class Planet
{
    public Planet planet { get; set; }
    public string name { get; set; }
    public string pictureId { get; set; }
    public string id { get; set; }

    public int metal { get; set; }
    public int crystal { get; set; }
    public int gas { get; set; }
    public int energy { get; set; }
    public int ozone { get; set; }

    public int pop { get; set; }

    public int popUsed { get; set; }

    public int defPopUsed { get; set; }

    public double metalPercent { get; set; }
    public double gasPercent { get; set; }
    public double crystalPercent { get; set; }
    public double popUsedPercent { get; set; }
    public double defPopPercent { get; set; }

    public int recycle { get; set; }

    public float storage { get; set; }

    public Vector3 coordinates { get; set; }

    public bool ozoneProblem { get; set; }

    public bool ozoneWarning { get; set; }

    public bool isAttacked { get; set; }

    public int IsTeam { get; set; }

    public Planet(JsonData planetInfo)
    {
        if (!planetInfo.Keys.Contains("name"))
        {
            return;
        }
        name = planetInfo["name"].ToString();
        pictureId = planetInfo["picture"].ToString();
        ozone = Int32.Parse(planetInfo["ozone"].ToString());
        id = planetInfo["id"].ToString();
        metal = Int32.Parse(planetInfo["metal"].ToString());
        crystal = Int32.Parse(planetInfo["crystal"].ToString());
        gas = Int32.Parse(planetInfo["gas"].ToString());
        pop = Int32.Parse(planetInfo["pop"].ToString());
        popUsed = Int32.Parse(planetInfo["pop_used"].ToString());
        defPopUsed = Int32.Parse(planetInfo["defence_pop_used"].ToString());
        energy = Int32.Parse(planetInfo["free_energy"].ToString());
        recycle = Int32.Parse(planetInfo["recycle"].ToString());
        coordinates = new Vector3(Int32.Parse(planetInfo["c1"].ToString()), Int32.Parse(planetInfo["c2"].ToString()), Int32.Parse(planetInfo["c3"].ToString()));
        storage = float.Parse(planetInfo["storage_used_percent"].ToString());
        ozoneProblem = (bool)planetInfo["ozone_problem"];
        ozoneWarning = (bool)planetInfo["ozone_warning"];
        IsTeam = Int32.Parse(planetInfo["is_team"].ToString());
        //ozoneProblem = true;

        isAttacked = false;

        metalPercent = ((double)metal / Double.Parse(planetInfo["max_metal"].ToString()));
        crystalPercent = ((double)crystal / Double.Parse(planetInfo["max_crystal"].ToString()));
        gasPercent = ((double)gas / Double.Parse(planetInfo["max_gas"].ToString()));
        popUsedPercent = ((double)popUsed / Double.Parse(planetInfo["pop"].ToString()));
        defPopPercent = (double)defPopUsed / Double.Parse(planetInfo["pop"].ToString());
    }
}
