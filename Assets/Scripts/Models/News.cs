﻿using UnityEngine;
using System.Collections;
using LitJson;
using UnityEngine.UI;

public class News : MonoBehaviour
{
    public string parsedJSON;

    // Use this for initialization
    public void getNews()
    {
        NetworkManager.instance.getLatestNews("mobile", "1", onNewsReceived);
    }


    private void onNewsReceived(JsonData response)
    {
        parsedJSON = response[0]["content"].ToString();
        //Debug.Log(parsedJSON);

        parsedJSON.Replace("<br />", "\n");
        parsedJSON = StripTagsCharArray(parsedJSON);

        GameObject.FindGameObjectWithTag(Constants.TAG_NEWS).GetComponent<Text>().text = parsedJSON;
    }


    public static string StripTagsCharArray(string source)
    {
        char[] array = new char[source.Length];
        int arrayIndex = 0;
        bool inside = false;

        for (int i = 0; i < source.Length; i++)
        {
            char let = source[i];
            if (let == '<')
            {
                inside = true;
                continue;
            }
            if (let == '>')
            {
                inside = false;
                continue;
            }
            if (!inside)
            {
                array[arrayIndex] = let;
                arrayIndex++;
            }
        }
        return new string(array, 0, arrayIndex);
    }
}
