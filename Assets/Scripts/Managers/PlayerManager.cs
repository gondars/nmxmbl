﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance = null;

    public Planet[] planets;
    public Building[] buildings;
    public Science[] sciences;
    public Upgrade[] upgrades;
    public Ship[] ships;
    public CommanderShip[] comShips;
    public Defence[] defences;
    public Dictionary<string, Message> messages;
    public Attack[] attacks;
    public int attacksNumber;
    public string raceId;


    public string PlayerName { get; set; }
    public string SessionToken { get; set; }
    public string Credits { get; set; }
    public string Stars { get; set; }
    public string Capsules { get; set; }

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
