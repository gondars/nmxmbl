﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System;
using System.Collections.Generic;
using System.Threading;
using LitJson;

public class LanguageManager : ScriptableObject
{
    private static LanguageManager instance = null;

    private const string enPath = "http://forums.incuvationgames.com/images/nmx/mobile/";

    private XmlDocument mainDoc = null;
    private XmlElement root = null;
    private string languagePath = string.Empty;
    private string[] languageFiles = null;

    public string currentLocale;
    public List<string> languages;

    public static LanguageManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = CreateInstance<LanguageManager>();
            }

            return instance;
        }
    }

    void Awake()
    {
        //languagePath = Application.dataPath + "/Languages/";
        //CollectLanguages();
        languages = new List<string>();
        languages.Add("en");
        languages.Add("bg");
    }

    // Use this for initialization
    void Start()
    {

    }

    private string GetLanguageFile(string language)
    {
        foreach (string langGo in languageFiles)
        {
            if (langGo.EndsWith(language + ".xml"))
            {
                return langGo;
            }
        }

        return string.Empty;
    }

    public void LoadLanguage(string language)
    {
        if (PlayerManager.instance.SessionToken != null && PlayerManager.instance.SessionToken != "")
        {

            NetworkManager.instance.SetServerLanguage(PlayerManager.instance.SessionToken, "sk", "1", language, OnLanguageSet);

        }

        try
        {
            //string loadFile = GetLanguageFile(language);
            mainDoc = new XmlDocument();
            //XmlTextReader reader = new XmlTextReader(enPath);
            mainDoc.Load(enPath + language + ".xml");
            root = mainDoc.DocumentElement;
            //StreamReader reader = new StreamReader(loadFile);
            //mainDoc.Load(enPath);
            //root = mainDoc.DocumentElement;
            //reader.Close();
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            LoadLanguage(language);
            return;
        }
        currentLocale = language;

        if (GameManager.instance.CurrentView == null)
        {
            if (LocalDataManager.instance.LoadData("rememberMe") == "true" && LocalDataManager.instance.LoadData("sessionToken") != null)
            {
                PlayerManager.instance.SessionToken = LocalDataManager.instance.LoadData("sessionToken");
                GameManager.instance.NavigateToView(GameManager.instance.gameView);
            }
            else
            {
                GameManager.instance.NavigateToView(GameManager.instance.loginView);
            }
        }
        else
        {
            LocalizedText[] texts = FindObjectsOfType<LocalizedText>();

            foreach (LocalizedText text in texts)
            {
                text.LocalizeText();
            }
        }

        LocalizedText[] textFields = FindObjectsOfType<LocalizedText>();
        foreach (LocalizedText text in textFields)
        {
            text.LocalizeText();
        }



    }


    private void OnLanguageSet(JsonData response = null)
    {
        return;
        //    try
        //    {
        //        //string loadFile = GetLanguageFile(language);
        //        mainDoc = new XmlDocument();
        //        //XmlTextReader reader = new XmlTextReader(enPath);
        //        mainDoc.Load(enPath + language + ".xml");
        //        root = mainDoc.DocumentElement;
        //        //StreamReader reader = new StreamReader(loadFile);
        //        //mainDoc.Load(enPath);
        //        //root = mainDoc.DocumentElement;
        //        //reader.Close();
        //    }
        //    catch (System.Exception e)
        //    {
        //        Debug.Log(e.Message);

        //        return;
        //    }
        //currentLocale = language;

        //if (GameManager.instance.CurrentView == null)
        //{
        //    GameManager.instance.NavigateToView(GameManager.instance.loginView);
        //}
        //else
        //{
        //    LocalizedText[] texts = FindObjectsOfType<LocalizedText>();

        //    foreach (LocalizedText text in texts)
        //    {
        //        text.LocalizeText();
        //    }
        //}

        //LocalizedText[] textFields = FindObjectsOfType<LocalizedText>();
        //foreach (LocalizedText text in textFields)
        //{
        //    text.LocalizeText();
        //}
    }


    public string GetString(string id, string param = "")
    {
        if (currentLocale == null)
        {
            return id;
        }
        XmlNode node = root.SelectSingleNode("//*[@id='" + id + "']");

        if (node == null)
        {
            return id;
        }
        else
        {
            string value = node.InnerText;
            if (node.Attributes["special"] != null && node.Attributes["special"].Value == "true")
            {
                value = ParseSpecialString(value, param);
            }
            value = value.Replace("\\n", "\n"); //replace new lines
            return value;
        }
    }

    public bool CheckIfSpecial(string id)
    {
        if (currentLocale == null)
        {
            return false;
        }
        XmlNode node = root.SelectSingleNode("//*[@id='" + id + "']");

        if (node == null)
        {
            return false;
        }
        else
        {
            if (node.Attributes["special"] != null && node.Attributes["special"].Value == "true")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private string ParseSpecialString(string text, string replaceWith)
    {
        return text.Replace("{|p|}", replaceWith);
    }

    //public string Get(string path)
    //{
    //    try
    //    {
    //        XmlNode node = root.SelectSingleNode(path);
    //        if (node == null)
    //        {
    //            return path;
    //        }
    //        else
    //        {
    //            string value = node.InnerText;
    //            value = value.Replace("\\n", "\n");
    //            return value;
    //        }
    //    }
    //    catch (System.Exception e)
    //    {
    //        Debug.Log(e.Message);
    //        return path;
    //    }
    //}
}
