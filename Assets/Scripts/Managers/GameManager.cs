﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using LitJson;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject loginView;
    public GameObject gameView;
    public GameObject planetView;
    public GameObject actionView;
    public GameObject attackView;
    public GameObject dynamicView;
    public GameObject menuView;
    public GameObject CurrentView;

    public GameObject background;

    public IDictionary<string, string[]> universes;
    public string actionType;
    public int chosenPlanetID;

    public News news;

    public const int NORMAL_BG = 0;
    public const int SAFE_BG = 1;
    public const int ATTACKED_BG = 2;

    public const string SUCCESS = "SUCCESS";
    public const string ERROR = "ERROR";


    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        universes = new Dictionary<string, string[]>();
        //GameObject.FindGameObjectWithTag(Constants.TAG_ADVISERTEXT).GetComponent<Text>().text = Constants.ADV_HELLO ;

        //news = new News();
        //news.getNews();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeView(GameObject fromView, GameObject toView)
    {
        fromView.SetActive(false);
        toView.SetActive(true);
        CurrentView = toView;
    }

    public void NavigateToView(GameObject view)
    {
        view.SetActive(true);
        CurrentView = view;
    }

    public void EnableDynamicView(string text, string type, UnityEvent e = null)
    {
        dynamicView.SetActive(true);
        dynamicView.GetComponent<DynamicViewController>().SetFunctionality(text, type, e);
    }

    public void DisableDynamicView()
    {
        dynamicView.SetActive(false);
    }

    public void EnableMenuView()
    {
        menuView.SetActive(!menuView.active);

    }

    public void ChangeBackground(int index)
    {
        background.GetComponent<ElementResizer>().ChangeBg(index);
    }

    // TODO extract to ErrorHandlerManager
    public string HandleResponse(JsonData response)
    {
        IDictionary r = response as IDictionary;
        if (r.Contains("success"))
        {
            return SUCCESS;
        }
        else if (r.Contains("error"))
        {
            return ERROR;
        }
        else
        {
            return ERROR;
        }
    }
}
