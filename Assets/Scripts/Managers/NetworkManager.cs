﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using LitJson;
using UnityEngine.UI;
using System.Net;
using System;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance = null;

    public string gameId = "sk";
    public string server = "1";

    public const string baseUrl = "http://50.game.nemexius.sk/api/mobile/endpoint.php";

    public string parsedJSON;
    public byte[] result;
    public JsonData data;

    public delegate void CallbackEventHandler(JsonData response);
    public event CallbackEventHandler Callback;

    public delegate void CallbackTextureHandler(Texture2D response);
    public event CallbackTextureHandler CallbackTexture;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }


    void Start()
    {
        // this will be called in the class we need to use it
        //getLatestNews("bg", "1", "bla");

        Connect();
    }

    public void Connect()
    {
        StartCoroutine(CheckInternetConnection());
    }

    IEnumerator CheckInternetConnection()
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null) // same as www.isError
        {
            //action(false);
            GameManager.instance.EnableDynamicView("No Internet Connection", "noConnection");
        }
        else
        {
            if (LanguageManager.Instance.currentLocale == null)
            {
                LanguageManager.Instance.LoadLanguage("en");
                StartCoroutine(GetServers(SetServers, true));
            }
        }
    }

    private void SetServers(JsonData response)
    {
        JsonData universe_list = response["universe_list"];
        foreach (DictionaryEntry entry in universe_list)
        {
            JsonData universes = universe_list[entry.Key.ToString()];

            foreach (DictionaryEntry worlds in universes)
            {
                if (worlds.Key.ToString() == "world_list")
                {
                    JsonData worlds_list = universes[worlds.Key.ToString()];
                    string[] worlds_Array = new string[worlds_list.Count];
                    for (int i = 0; i < worlds_list.Count; i++)
                    {

                        worlds_Array[i] = worlds_list[i].ToString();
                    }
                    GameManager.instance.universes[entry.Key.ToString().ToLower()] = worlds_Array;
                }

            }
        }
    }


    IEnumerator GetServers(CallbackEventHandler callback, bool debug = false)
    {
        // https://www.doubleaxion.com/portal/status/?game=nmx
        Dictionary<string, string> urlheaders = new Dictionary<string, string>();

        urlheaders["game"] = "nmx";


        WWW www = new WWW("https://www.doubleaxion.com/portal/status/?game=nmx", null, urlheaders);
        yield return www;

        if (www.error != null)
        {
            if (debug) Debug.Log(www.error);
        }
        else
        {
            if (debug) Debug.Log(www.text);
            //DebugConsole.Log(www.text);
            data = JsonMapper.ToObject(www.text);
            callback(data);
        }
    }


    public void SetServerLanguage(string token, string gameId, string server, string lang, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "lang", lang
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Language_Change", callback, true, parameters, headers));
    }


    public void getLatestNews(string gameId, string limit, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", gameId,
            "limit", limit
        };

        StartCoroutine(sendRequest("?method=News_Fetch", callback, false, parameters));
    }


    public void login(string username, string password, string gameId, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "user", username,
            "pass", password,
            "game_id", this.gameId,
            "world_id", this.server
        };

        StartCoroutine(sendRequest("?method=auth_login", callback, false, parameters, null, null, false));
    }


    public void register(string username, string password, string email, string locale, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "username", username,
            "password", password,
            "email", email,
            "locale", locale,
            "server", server
        };

        //TODO the request itself
    }


    public void getPlayerPlanets(string token, string gameId, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Player_Planets_Fetch", callback, true, parameters, headers, null, false));
    }


    public void getPlayerMessages(string token, string gameId, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=player_messages_fetch", callback, true, parameters, headers));
    }


    public void getPlayerAttacks(string token, string gameId, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=player_attacks_fetch", callback, true, parameters, headers, null, false));
    }


    public void GetCapsules(string token, string gameId, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Premium_Capsules_Fetch", callback, true, parameters, headers));
    }


    public void getBuildings(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=player_buildings_fetch", callback, true, parameters, headers, null, false));
    }

    // Upgrade Building
    public void ConstructBuilding(string token, string gameId, string server, string planetId, string buildingId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId,
            "building_id", buildingId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token,

        };

        StartCoroutine(sendRequest("?method=Building_Construct", callback, true, parameters, headers, null, true));
    }

    // cancel upgrade building
    public void CancelBuildingUpgrade(string token, string gameId, string server, string processId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "process_id", processId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Building_Cancel", callback, true, parameters, headers, null, false));
    }

    //demolish building
    public void DemolishBuilding(string token, string gameId, string server, string buildingId, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "building_id", buildingId,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Building_Demolish", callback, true, parameters, headers, null, false));
    }
    //ship upgrades
    public void GetUpgrades(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Upgrade_Fetch", callback, true, parameters, headers, null, false));
    }

    public void UpgradeShip(string token, string gameId, string server, string shipId, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "ship_id", shipId,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Upgrade_Start", callback, true, parameters, headers, null, false));
    }

    public void CancelUpgrades(string token, string gameId, string server, string processId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "process_id", processId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Upgrade_Cancel", callback, true, parameters, headers, null, false));
    }
    //sciences
    public void GetSciences(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
        dicHeaders["Authorization"] = "Basic: " + token;

        StartCoroutine(sendRequest("?method=Player_Sciences_Fetch", callback, true, parameters, null, dicHeaders, false));
    }

    public void UpgradeSciences(string token, string gameId, string server, string scienceId, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "science_id", scienceId,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Sciences_Research", callback, true, parameters, headers, null, false));
    }

    public void CancelSciences(string token, string gameId, string server, string processId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "process_id", processId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Sciences_Cancel", callback, true, parameters, headers, null, false));
    }
    //ships
    public void GetShips(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Shipyard_Ships_Fetch", callback, true, parameters, headers, null, false));
    }

    public void TrainShip(string token, string gameId, string server, string shipId, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id",this.server,
            "ship_id", shipId,
            "planet_id", planetId,
            "count", "1"
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Shipyard_Ships_Train", callback, true, parameters, headers, null, false));
    }

    public void CancelShipTraining(string token, string gameId, string server, string processId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id",this.server,
            "process_id", processId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Shipyard_Ships_Cancel", callback, true, parameters, headers, null, false));
    }

    //Commanderships
    public void GetCommanderShips(string token, string planetId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("planet_id", planetId);

        StartCoroutine(sendPostRequest("?method=Shipyard_CommanderShips_Fetch", callback, headers, form, false));
    }

    public void TrainCommanderShip(string token, int comShipId, string planetId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("commander_ship_id", comShipId);
        form.AddField("planet_id", planetId);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Train", callback, headers, form, false));
    }

    public void BuyCommanderShip(string token, int comShipId, string planetId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("commander_ship_id", comShipId);
        form.AddField("planet_id", planetId);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Buy", callback, headers, form, false));
    }

    public void CancelCommanderShip(string token, string processId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("process_id", processId);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Cancel", callback, headers, form, false));
    }

    public void GetCommanderShipQueue(string token, CallbackEventHandler callback)
    {
        string[] headers =
          {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Queue", callback, headers, form, false));
    }

    public void CommanderShipUpgradeFetch(string token, CallbackEventHandler callback)
    {
        string[] headers =
          {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Upgrade_Fetch", callback, headers, form, true));
    }

    public void StartCommanderShipUpgrade(string token, int comShipId, string planetId, CallbackEventHandler callback)
    {
        string[] headers =
          {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("commander_ship_id", comShipId);
        form.AddField("planet_id", planetId);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Upgrade_Start", callback, headers, form, false));
    }

    public void BuyCommanderShipUpgrade(string token, int comShipId, string planetId, CallbackEventHandler callback)
    {
        string[] headers =
          {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("commander_ship_id", comShipId);
        form.AddField("planet_id", planetId);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Upgrade_Buy", callback, headers, form, false));
    }

    public void CancelCommanderShipUpgrade(string token, string processId, CallbackEventHandler callback)
    {
        string[] headers =
         {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);
        form.AddField("process_id", processId);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Upgrade_Cancel", callback, headers, form, false));
    }

    public void GetCommanderShipUpgradeQueue(string token, CallbackEventHandler callback)
    {
        string[] headers =
         {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();
        form.AddField("game_id", gameId);
        form.AddField("world_id", server);

        StartCoroutine(sendPostRequest("?method=Shipyard_Commanderships_Upgrade_Queue", callback, headers, form, false));
    }
    //Defence

    public void GetDefence(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Shipyard_Defence_Fetch", callback, true, parameters, headers, null, false));
    }

    public void TrainDefence(string token, string gameId, string server, string defenceId, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id",this.server,
            "defence_id", defenceId,
            "planet_id", planetId,
            "count", "1"
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Shipyard_Defence_Train", callback, true, parameters, headers, null, false));
    }

    public void CancelDefenceTrain(string token, string gameId, string server, string processId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id",this.server,
            "process_id", processId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Shipyard_Defence_Cancel", callback, true, parameters, headers, null, false));
    }

    public void SpeedUp(string token, string gameId, string server, string processId, string processType, string confirmed, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "process_id", processId,
            "process_type", processType,
            "confirmed", confirmed
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Process_Boost", callback, true, parameters, headers, null, false));
    }

    public void GetProcessQueue(string token, string gameId, string server, string processType, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "process_type", processType
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Process_Queue", callback, true, parameters, headers, null, false));
    }

    public void GetAllUpgrades(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server,
            "planet_id", planetId
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Player_Technics_Fetch", callback, true, parameters, headers));
    }

    //Attack Screen
    public void GetOpponents(string token, string gameId, string server, CallbackEventHandler callback)
    {
        string[] parameters =
        {
            "game_id", this.gameId,
            "world_id", this.server
        };

        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        StartCoroutine(sendRequest("?method=Search_Fetch", callback, true, parameters, headers, null, true));
    }


    public void CheckFleets(string token, string gameId, string server, string planetId, string type, Dictionary<string, string> ships, Dictionary<string, string> commanderShip, string raceId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();

        string ships_json = "{\"" + raceId + "\": {";
        bool isFirstElement = true;
        foreach (var elem in ships)
        {
            if (!isFirstElement)
            {
                ships_json += ",";
            }
            ships_json += "\"" + elem.Key + "\":\"" + elem.Value + "\"";
            isFirstElement = false;
        }

        ships_json += "}}";

        string commanderShips_json = "{";
        foreach (var elem in commanderShip)
        {
            commanderShips_json += elem.Key + ":" + elem.Value + ",";
        }
        commanderShips_json += "}";

        form.AddField("game_id", this.gameId);
        form.AddField("world_id", this.server);
        form.AddField("planet_id", planetId);
        form.AddField("ship", ships_json);
        form.AddField("commanderShip", commanderShips_json);
        form.AddField("type", type);

        StartCoroutine(sendPostRequest("?method=Fleets_Check", callback, headers, form, true));
    }


    public void SendFleets(string token, string gameId, string server, string planetId, string type, Dictionary<string, string> ships, Dictionary<string, string> commanderShip, string raceId,
        string[] coordinates, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();

        string ships_json = "{\"" + raceId + "\": {";
        bool isFirstElement = true;
        foreach (var elem in ships)
        {
            if (!isFirstElement)
            {
                ships_json += ",";
            }
            ships_json += "\"" + elem.Key + "\":\"" + elem.Value + "\"";
            isFirstElement = false;
        }

        ships_json += "}}";
        Debug.Log(ships_json);

        string commanderShips_json = "{";
        foreach (var elem in commanderShip)
        {
            commanderShips_json += elem.Key + ":" + elem.Value;
        }
        commanderShips_json += "}";

        Debug.Log(commanderShips_json);

        form.AddField("game_id", this.gameId);
        form.AddField("world_id", this.server);
        form.AddField("planet_id", planetId);
        form.AddField("ship", ships_json);
        form.AddField("commanderShip", commanderShips_json);
        form.AddField("type", type);
        form.AddField("c1", coordinates[0]);
        form.AddField("c2", coordinates[1]);
        form.AddField("c3", coordinates[2]);
        form.AddField("speed", "10");
        form.AddField("speed_motivation", "0");
        form.AddField("metal", "0");
        form.AddField("crystal", "0");
        form.AddField("gas", "0");
        form.AddField("scrap", "0");
        form.AddField("battle_rounds", "12");
        form.AddField("scrap_motivation", "0");
        form.AddField("flight_hours", "0");
        form.AddField("flight_minutes", "5");

        StartCoroutine(sendPostRequest("?method=Fleets_Send", callback, headers, form, true));
    }


    public void GetFleets(string token, string gameId, string server, string planetId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();

        form.AddField("game_id", this.gameId);
        form.AddField("world_id", this.server);
        form.AddField("planet_id", planetId);

        StartCoroutine(sendPostRequest("?method=Fleets_Fetch", callback, headers, form, true));
    }


    public void FleetsDetails(string token, string gameId, string server, string planetId, string fleetId, CallbackEventHandler callback)
    {
        string[] headers =
        {
            "Authorization", "Basic: " + token
        };

        WWWForm form = new WWWForm();

        form.AddField("game_id", this.gameId);
        form.AddField("world_id", this.server);
        form.AddField("planet_id", planetId);
        form.AddField("fleet_id", fleetId);

        StartCoroutine(sendPostRequest("?method=Fleets_Details", callback, headers, form, true));
    }


    IEnumerator sendPostRequest(string action, CallbackEventHandler callback, string[] headers = null, WWWForm ads = null, bool debug = false)
    {
        string url = baseUrl;
        url += action;

        if (debug) Debug.Log(url);

        Dictionary<string, string> urlheaders = new Dictionary<string, string>();



        if (headers != null)
        {
            for (int i = 0; i < headers.Length; i++)
            {
                urlheaders[headers[i]] = headers[++i];
            }
        }

        WWW www = new WWW(url, ads.data, urlheaders);

        yield return www;

        if (www.error != null)
        {
            if (debug) Debug.Log(www.error);
        }
        else
        {
            if (debug) Debug.Log(www.text);
            //DebugConsole.Log(www.text);
            data = JsonMapper.ToObject(www.text);
            callback(data);
        }
    }


    IEnumerator sendRequest(string action, CallbackEventHandler callback, bool isPost, string[] parameters, string[] headers = null, Dictionary<string, string> setHeaders = null, bool debug = false)
    {
        string url = baseUrl;
        url += action;

        for (int i = 0; i < parameters.Length; i++)
        {
            if (!isPost)
            {
                url += "&" + parameters[i] + "=" + parameters[++i]; // gets element and the one after it, and adds +1 to "i" in the same time not to repeat
            }
            else
            {
                // TODO post
                url += "&" + parameters[i] + "=" + parameters[++i];
            }
        }

        if (debug) Debug.Log(url);

        if (!isPost)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            yield return www.Send();

            if (www.isError)
            {
                if (debug) Debug.Log(www.error);
                GameManager.instance.EnableDynamicView(www.error, "noConnection");
            }
            else
            {
                if (debug) Debug.Log(www.downloadHandler.text);

                data = JsonMapper.ToObject(www.downloadHandler.text);

                callback(data);

                // this will be returned to the Callback function, its here now for testing only
                //parsedJSON = data[0]["content"].ToString();
                //Debug.Log(parsedJSON);
            }
        }
        else
        {
            Dictionary<string, string> urlheaders = new Dictionary<string, string>();



            if (headers != null)
            {
                for (int i = 0; i < headers.Length; i++)
                {
                    urlheaders[headers[i]] = headers[++i];
                }
            }

            if (setHeaders != null)
            {
                urlheaders = setHeaders;
            }

            WWW www = new WWW(url, null, urlheaders);
            yield return www;

            if (www.error != null)
            {
                if (debug) Debug.Log(www.error);
            }
            else
            {
                if (debug) Debug.Log(www.text);
                //DebugConsole.Log(www.text);
                data = JsonMapper.ToObject(www.text);
                callback(data);
            }
        }
    }


    public IEnumerator getTexture(string planetId, CallbackTextureHandler callback)//(Sprite container)
    {
        WWW www = new WWW("http://static.nemexia.net/game/img/planets/normal/planet_" + planetId + ".png");

        yield return www;

        //Texture2D sprites = www.texture;
        //Rect rec = new Rect(0, 0, sprites.width, sprites.height);
        //container = Sprite.Create(www.texture, rec, new Vector2(0, 0));
        callback(www.texture);



        //if (www.error != null)
        //{
        //    Debug.Log(www.texture);
        //    Texture2D sprites = www.texture;
        //    Rect rec = new Rect(0, 0, sprites.width, sprites.height);
        //    container = Sprite.Create(www.texture, rec, new Vector2(0, 0));

        //    //Debug.Log(container);
        //}
        //else
        //{
        //    Debug.Log(www.error);
        //}
    }

    public IEnumerator loadImage(string url, CallbackTextureHandler callback)
    {
        WWW www = new WWW(url);

        yield return www;

        callback(www.texture);
    }
}