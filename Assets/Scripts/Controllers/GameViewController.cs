﻿using UnityEngine;
using LitJson;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Timers;

public class GameViewController : MonoBehaviour
{

    public LocalizedText IncomingAttacksContainer;
    public Text MessagesContainer;
    public Text attackTime;
    public GameObject safePanel;
    public GameObject attackedPanel;


    private List<string> adviserMessages;

    bool messagesLoaded = false;
    bool attacksLoaded = false;
    bool planetsLoaded = false;

    Timer attackTimer; 
    TimeSpan closestAttackTime;

    private int totalOzoneWarnings;

    // Use this for initialization
    void Start()
    {
        //NetworkManager.instance.getPlayerPlanets(PlayerManager.instance.SessionToken, "sk", "1", onPlanetsInfo);
        //NetworkManager.instance.getPlayerMessages(PlayerManager.instance.SessionToken, "sk", "1", onMessagesInfo);
        //NetworkManager.instance.getPlayerAttacks(PlayerManager.instance.SessionToken, "sk", "1", onAttackInfo);
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerManager.instance.attacksNumber > 0)
        {
            attackTime.text = closestAttackTime.ToString();
        }  
    }

    void OnEnable()
    {
        SetBg();

        LocalizedText[] textFields = FindObjectsOfType<LocalizedText>();
        foreach (LocalizedText text in textFields)
        {
            text.LocalizeText();
        }

        attackTimer = new Timer(1000);
        attackTimer.Elapsed += new ElapsedEventHandler(OnAttackTicker);
        attackTimer.Enabled = true;

        closestAttackTime = new TimeSpan();

        messagesLoaded = false;
        attacksLoaded = false;
        planetsLoaded = false;
        InvokeRepeating("GetViewInfo", 0.0f, 10.0f);
        //NetworkManager.instance.getBuildings(PlayerManager.instance.SessionToken, "sk", "1", OnBuildingsInfo);
    }

    void OnDisable()
    {
        attackTimer.Stop();
        attackTimer.Dispose();
        CancelInvoke("GetViewInfo");
    }

    private void SetBg()
    {
        if (PlayerManager.instance.attacksNumber > 0)
        {
            GameManager.instance.ChangeBackground(GameManager.ATTACKED_BG);
            attackedPanel.SetActive(true);
            safePanel.SetActive(false);
        }
        else 
        {
            GameManager.instance.ChangeBackground(GameManager.SAFE_BG);
            attackedPanel.SetActive(false);
            safePanel.SetActive(true);
        }
    }

    private void GetViewInfo()
    {
        totalOzoneWarnings = 0;
        NetworkManager.instance.getPlayerPlanets(PlayerManager.instance.SessionToken, "sk", "1", onPlanetsInfo);
        NetworkManager.instance.getPlayerMessages(PlayerManager.instance.SessionToken, "sk", "1", onMessagesInfo);
        NetworkManager.instance.getPlayerAttacks(PlayerManager.instance.SessionToken, "sk", "1", onAttackInfo);
    }

    private void OnBuildingsInfo(JsonData response)
    {

    }

    public void onPlanetsInfo(JsonData response)
    {
        PlayerManager.instance.planets = new Planet[response.Count];
        IList list = response;
        int i = 0;
        //JsonData data = response["1"];
        //foreach (JsonData elem in response as IList)
        //{
        //    PlayerManager.instance.planets[i] = new Planet(elem);

        //    if(PlayerManager.instance.planets[i].ozoneWarning)
        //    {
        //        totalOzoneWarnings++;
        //    }

        //    i++;
        //}

        foreach (DictionaryEntry elem in response)
        {
            PlayerManager.instance.planets[i] = new Planet(elem.Value as JsonData);

            if (PlayerManager.instance.planets[i].ozoneWarning)
            {
                totalOzoneWarnings++;
            }

            i++;
        }

        i = 0;

        planetsLoaded = true;

        if (messagesLoaded && totalOzoneWarnings > 0)
        {
            attachOzoneProblemMessage();
        }
        else if (messagesLoaded && totalOzoneWarnings == 0)
        {
            SetAdviserText();
        }

        if (attacksLoaded)
        {
            setAttacksIndicators();
            createPlanets();
        }
    }

    public void onMessagesInfo(JsonData response)
    {
        adviserMessages = new List<string>();
        adviserMessages.Add("Welcome_Back");
        adviserMessages.Add("\n\n");

        PlayerManager.instance.messages = new Dictionary<string, Message>();

        foreach (DictionaryEntry elem in response)
        {
            //(elem.Value as JsonData)["new"] = 2;  // for testing
            if (Int32.Parse((elem.Value as JsonData)["new"].ToString()) > 0 && (elem.Key.ToString() == "3" || elem.Key.ToString() == "2" || elem.Key.ToString() == "1"))
            {
                PlayerManager.instance.messages[elem.Key.ToString()] = new Message(elem.Value);
            }

        }
        messagesLoaded = true;

        if (PlayerManager.instance.messages.Count == 0)
        {
            //text += "You don't have new messages.";
        }
        else
        {
            adviserMessages.Add("Adv_Msg_1");
        }

        int counter = 1;

        foreach (KeyValuePair<string, Message> message in PlayerManager.instance.messages)
        {
            if (message.Value.newM > 0)
            {
                attachText(message.Key, message.Value.newM);
                if (counter < PlayerManager.instance.messages.Count - 1)
                {
                    adviserMessages.Add(", ");
                }
                else if (counter == PlayerManager.instance.messages.Count - 1)
                {
                    adviserMessages.Add(" ");
                    adviserMessages.Add("And");
                    adviserMessages.Add(" ");
                }
                else
                {
                    adviserMessages.Add(".");
                }
                counter++;
            }

        }

        if (planetsLoaded && totalOzoneWarnings > 0)
        {
            attachOzoneProblemMessage();
        }
        else if (messagesLoaded && totalOzoneWarnings == 0)
        {
            SetAdviserText();
        }
    }

    public void onAttackInfo(JsonData response)
    {
        // test 2 attacks, just set the proper cordinates
        //string data = "[{\"id\":\"954693\",\"at_id\":\"544\",\"de_id\":\"725\",\"at_pl_id\":\"1256\",\"de_pl_id\":\"64\",\"type\":\"3\",\"at_c1\":\"3\",\"at_c2\":\"11\",\"at_c3\":\"9\",\"de_c1\":\"3\",\"de_c2\":\"5\",\"de_c3\":\"6\",\"wait_time\":\"12\",\"time\":\"441\",\"forward_time\":\"2016-10-29 07:58:17\",\"backward_time\":\"2016-10-29 08:05:38\",\"metal\":\"0\",\"crystal\":\"0\",\"gas\":\"0\",\"scrap\":\"0\",\"cargo\":\"500000\",\"status\":\"0\",\"process\":\"0\",\"scrap_motivation\":\"0\"},{\"id\":\"954693\",\"at_id\":\"544\",\"de_id\":\"725\",\"at_pl_id\":\"1256\",\"de_pl_id\":\"63\",\"type\":\"3\",\"at_c1\":\"3\",\"at_c2\":\"11\",\"at_c3\":\"9\",\"de_c1\":\"3\",\"de_c2\":\"5\",\"de_c3\":\"6\",\"wait_time\":\"12\",\"time\":\"441\",\"forward_time\":\"2016-10-29 07:58:17\",\"backward_time\":\"2016-10-29 08:05:38\",\"metal\":\"0\",\"crystal\":\"0\",\"gas\":\"0\",\"scrap\":\"0\",\"cargo\":\"500000\",\"status\":\"0\",\"process\":\"0\",\"scrap_motivation\":\"0\"}]";
        //response = JsonMapper.ToObject(data);

        PlayerManager.instance.attacks = new Attack[response.Count];
        PlayerManager.instance.attacksNumber = response.Count;

        SetBg();

        if (PlayerManager.instance.attacksNumber == 0)
        {
            closestAttackTime = TimeSpan.FromSeconds(0);
        }

        int i = 0;
        
        foreach (JsonData elem in response as IList)
        {
            PlayerManager.instance.attacks[i] = new Attack(elem);

            // test
            string attackTime = elem["forward_time"].ToString();
            string serverTime = elem["server_time"].ToString();

            DateTime attackDate = DateTime.Parse(attackTime);
            DateTime serverDate = DateTime.Parse(serverTime);

            //Debug.Log(attackDate);

            TimeSpan attackInterval = attackDate - serverDate;

            if (TimeSpan.Compare(closestAttackTime, attackInterval) == 1 || i == 0)
            {
                closestAttackTime = attackInterval;
            }

            //Debug.Log("Interval: " + attackInterval);

            i++;
        }
        i = 0;


        //IncomingAttacksContainer.specialString = PlayerManager.instance.attacksNumber.ToString();
        //IncomingAttacksContainer.LocalizedID = "Inc_Attacks";
        attacksLoaded = true;

        if (planetsLoaded)
        {
            setAttacksIndicators();
            createPlanets();
        }
    }

    private void OnAttackTicker(object source, EventArgs eArgs)
    {
        
        if (TimeSpan.Compare(closestAttackTime, TimeSpan.FromSeconds(0)) > 0)
        {
            // TODO - set text timer

            //Debug.Log("attack incoming in: " + closestAttackTime);
            closestAttackTime = closestAttackTime.Subtract(TimeSpan.FromSeconds(1));
        }
    }

    private void createPlanets()
    {
        int planetsCreated = GameObject.FindGameObjectWithTag(Constants.PLANET_CONTAINER).transform.childCount;
        if (planetsCreated != PlayerManager.instance.planets.Length)
        {
            for (int a = 0; a < PlayerManager.instance.planets.Length - planetsCreated; a++)
            {
                GameObject planet = Instantiate(Resources.Load("Prefabs/Planet")) as GameObject;
            }
        }
    }

    private void setAttacksIndicators()
    {
        for (int a = 0; a < PlayerManager.instance.attacks.Length; a++)
        {
            for (int p = 0; p < PlayerManager.instance.planets.Length; p++)
            {
                if (PlayerManager.instance.attacks[a].dePlanetId == PlayerManager.instance.planets[p].id)
                {
                    PlayerManager.instance.planets[p].isAttacked = true;
                }
            }
        }
    }

    private void setPlanetImage(Texture response)
    {
        Debug.Log(response);

        //for
    }

    private void onMessageInfo()
    {
        string message = String.Format("Welcome back, Commander!\n\nWhile you were away you have received {0} messages with Battle reports, {1} Personal Messages and {2} System Messages");
        GameObject.FindGameObjectWithTag(Constants.MESSAGE_INFO).GetComponent<Text>().text = message;
    }

    private void attachOzoneProblemMessage()
    {
        adviserMessages.Add("\n\n");
        adviserMessages.Add("Adv_Msg_2");
        string text = "";

        int ozoneCounter = 1;
        for (int i = 0; i < PlayerManager.instance.planets.Length; i++)
        {
            if (PlayerManager.instance.planets[i].ozoneWarning)
            {
                text = String.Format("{0} ({1}:{2}:{3})", PlayerManager.instance.planets[i].name, PlayerManager.instance.planets[i].coordinates.x, PlayerManager.instance.planets[i].coordinates.y, PlayerManager.instance.planets[i].coordinates.z);
                adviserMessages.Add(text); // coordinates and names
                if (ozoneCounter < totalOzoneWarnings - 1)
                {
                    adviserMessages.Add(", ");
                }
                else if (ozoneCounter == totalOzoneWarnings - 1)
                {
                    adviserMessages.Add(" ");
                    adviserMessages.Add("And");
                    adviserMessages.Add(" ");
                }
                else
                {
                    adviserMessages.Add(" ");
                }
                ozoneCounter++;
            }
        }
        //adviserMessages.Add(text); // adds the coordinates and names of the planets with Ozone Warning
        adviserMessages.Add("Adv_Msg_3");

        SetAdviserText();
    }

    private void SetAdviserText()
    {
        //MessagesContainer.GetComponent<LocalizedText>().LocalizedIDs = adviserMessages;
    }

    private string attachText(string id, int val)
    {
        string result = "";

        switch (id)
        {
            case Constants.M_PERSONAL:
                result += val + " Personal Messages";
                adviserMessages.Add("MSG_Personal");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_ADMINISTRATIVE:
                result += val + " Administrative Messages";
                adviserMessages.Add("MSG_Administrative");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_REPORTS:
                result += val + " Reports";
                adviserMessages.Add("MSG_Reports");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_FLEETS:
                result += val + " Fleets Messages";
                adviserMessages.Add("MSG_Fleets");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_ALLIANCE:
                result += val + " Alliance Messages";
                adviserMessages.Add("MSG_Alliance");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_WALL:
                result += val + " Wall Messages";
                adviserMessages.Add("MSG_Wall");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_PAYMENTS:
                result += val + " Payments Messages";
                adviserMessages.Add("MSG_Payments");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_ARENA_REPORTS:
                result += val + " Arena Reports";
                adviserMessages.Add("MSG_ArenaReports");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_TEAM_REPORTS:
                result += val + " Team Reports";
                adviserMessages.Add("MSG_TeamReports");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_FEEDBACK:
                result += val + " Feedbacks";
                adviserMessages.Add("MSG_Feedback");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_SEND:
                result += val + " Send Messages";
                adviserMessages.Add("MSG_Send");
                adviserMessages.Add(val.ToString());
                break;
            case Constants.M_ACHIEVEMENTS:
                result += val + " Achievements";
                adviserMessages.Add("MSG_Achievements");
                adviserMessages.Add(val.ToString());
                break;
        }


        return result;
    }
}
