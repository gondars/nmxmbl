﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class DynamicViewController : MonoBehaviour
{

    public Text textUI;
    public Button leftBtnUI;
    public Button rightBtnUI;
    public string text;
    public string type;


    public void SetFunctionality(string txt, string type, UnityEvent e = null)
    {
        Debug.Log("Dynamic On");
        this.text = txt;
        this.type = type;

        textUI.text = text;

        leftBtnUI.onClick.RemoveAllListeners();
        rightBtnUI.onClick.RemoveAllListeners();

        switch (this.type)
        {
            case "logout":
                leftBtnUI.onClick.AddListener(() => LogoutAccept());
                rightBtnUI.onClick.AddListener(() => LogoutCancel());
                leftBtnUI.transform.GetChild(0).GetComponent<Text>().text = "Yes";
                rightBtnUI.transform.GetChild(0).GetComponent<Text>().text = "No";
                break;
            case "noConnection":
                leftBtnUI.onClick.AddListener(() => RetryConnection());
                rightBtnUI.onClick.AddListener(() => CloseGame());
                leftBtnUI.transform.GetChild(0).GetComponent<Text>().text = "Retry";
                rightBtnUI.transform.GetChild(0).GetComponent<Text>().text = "Close";
                break;
            case "speedUp":
                leftBtnUI.onClick.AddListener(() => e.Invoke());
                leftBtnUI.onClick.AddListener(() => disableView());
                rightBtnUI.onClick.AddListener(() => disableView());
                leftBtnUI.transform.GetChild(0).GetComponent<Text>().text = "Yes";
                rightBtnUI.transform.GetChild(0).GetComponent<Text>().text = "No";
                break;
            default:
                leftBtnUI.onClick.AddListener(() => disableView());
                rightBtnUI.onClick.AddListener(() => disableView());
                leftBtnUI.transform.GetChild(0).GetComponent<Text>().text = "OK";
                rightBtnUI.transform.GetChild(0).GetComponent<Text>().text = "Close";
                break;

        }
    }

    void LogoutAccept()
    {
        GameManager.instance.ChangeView(GameManager.instance.CurrentView, GameManager.instance.loginView);
    }

    void LogoutCancel()
    {
        disableView();
    }

    void CloseGame()
    {
        Debug.Log("Closing Game...");
        Application.Quit();
    }

    void RetryConnection()
    {
        NetworkManager.instance.Connect();
        disableView();
    }

    void disableView()
    {
        this.gameObject.SetActive(false);
    }

}
