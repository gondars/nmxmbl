﻿using UnityEngine;
using LitJson;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class LoginViewController : MonoBehaviour
{
    public string token;
    public string planet;
    public InputField username;
    public InputField password;
    //public InputField email;
    public Dropdown version;
    public Dropdown universe;

    public GameObject messagePanelText;
    public GameObject rememberMe;
    //public GameObject advisorIconPanel;
    //public GameObject advisorButtonPanel;
    //public GameObject loginPanel;
    //public GameObject registerPanel;
    //public News news;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnEnable()
    {
        LocalizedText[] textFields = FindObjectsOfType<LocalizedText>();
        foreach (LocalizedText text in textFields)
        {
            text.LocalizeText();
        }

        messagePanelText.GetComponent<Text>().text = "";
        //GameObject.FindGameObjectWithTag(Constants.TAG_ADVISERTEXT).GetComponent<LocalizedText>().localizedID = Constants.ADV_HELLO;

        //news = new News();
        //InvokeRepeating("GetNews", 0.0f, 10.0f);
        SetBg();
    }

    void OnDisable()
    {
        CancelInvoke("GetNews");
    }

    void GetNews()
    {
        //news.getNews();
    }

    private void SetBg()
    {
        GameManager.instance.ChangeBackground(GameManager.NORMAL_BG);
    }

    public void login()
    {
        // TODO set the Version properly
        NetworkManager.instance.login(username.text, password.text, "sk", "1", onLogin);
        //NetworkManager.instance.login("tulpas", "123456", "sk", "1", onLogin);
    }

    public void Register()
    {
        //NetworkManager.instance.register(username.text, password.text, email.text, "sk", "1", OnRegister);
    }

    public void onLogin(JsonData response)
    {
        //if (response["error"])
        //{
        //    Debug.Log(response["message"]);
        //    return;
        //}
        if (response[0].ToString() == "False")
        {
            PlayerManager.instance.SessionToken = response[1].ToString();
            //PlayerManager.instance.SessionToken = "m2tc1aam7po59mn5ieasab1f57";
            PlayerManager.instance.PlayerName = response[2]["user"].ToString();
            PlayerManager.instance.Credits = response[2]["credits"].ToString();
            PlayerManager.instance.Stars = response[2]["stars"].ToString();
            PlayerManager.instance.raceId = response[2]["race_id"].ToString();
            Debug.Log(PlayerManager.instance.SessionToken);
            if (rememberMe.GetComponent<Toggle>().isOn == true)
            {
                LocalDataManager.instance.SaveData("rememberMe", "true");
                LocalDataManager.instance.SaveData("sessionToken", PlayerManager.instance.SessionToken);
            }
            else
            {
                LocalDataManager.instance.SaveData("rememberMe", "false");
                LocalDataManager.instance.SaveData("sessionToken", null);
            }
            GameManager.instance.ChangeView(GameManager.instance.loginView, GameManager.instance.gameView);
        }
        else
        {
            Debug.Log("MESSAGE: " + response[1]);
            if (response[1].ToString() == "Invalid username and/or password")
            {
                WrongUsernameOrPassword();
            }
            else if (response[1].ToString() == "Cannot resolve destination host")
            {
                GameManager.instance.EnableDynamicView("Cannot connect to server", "noConnection");
            }

        }


        //PlayerManager.instance.SessionToken = token;
        //NetworkManager.instance.getPlayerPlanets(token, "sk", "1", onPlanetsInfo);
    }

    public void OnRegister(JsonData response)
    {
        Debug.Log("Register");
        //TODO
    }

    void WrongUsernameOrPassword()
    {
        messagePanelText.GetComponent<Text>().text = "Incorrect Username / Password ";
        //advisorIconPanel.SetActive(true);
        //advisorButtonPanel.SetActive(true);
        //GameObject.FindGameObjectWithTag(Constants.TAG_ADVISERTEXT).GetComponent<LocalizedText>().LocalizedIDs = new List<string> { Constants.ADV_SORRY, "\n\n", Constants.ADV_ERROR_1 };
    }

    public void AcceptRegister()
    {
        //loginPanel.SetActive(false);
        //advisorButtonPanel.SetActive(false);
        //registerPanel.SetActive(true);

        // TODO add locale for this
        //GameObject.FindGameObjectWithTag(Constants.TAG_ADVISERTEXT).GetComponent<Text>().text = Constants.ADV_REGISTER;
    }

    public void CancelRegister()
    {
        //advisorIconPanel.SetActive(false);
        //advisorButtonPanel.SetActive(false);
        //GameObject.FindGameObjectWithTag(Constants.TAG_ADVISERTEXT).GetComponent<LocalizedText>().LocalizedID = Constants.ADV_HELLO;
    }
    //public void onPlanetsInfo(JsonData response)
    //{
    //    Debug.Log(response[0]["player_id"]);
    //    PlayerManager.instance.planets = new Planet[response.Count];

    //    int i = 0;
    //    foreach(JsonData elem in response as IList)
    //    {
    //        PlayerManager.instance.planets[i] = new Planet(elem);
    //        i++;
    //    }
    //    i = 0;
    //}

    public void OnLanguage()
    {
        if (GameManager.instance.universes.Count == 0)
        {
            return;
        }
        LanguagePopUp languagepop = new LanguagePopUp();
        languagepop.Create();
    }


    public void OnUniverse()
    {
        if (GameManager.instance.universes.Count == 0)
        {
            return;
        }
        UniversePopUp universepop = new UniversePopUp();
        universepop.Create();
    }

}
