﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using LitJson;
using System;
using System.Collections.Generic;

public class PlanetViewController : MonoBehaviour
{
    public Text Attacks;
    public GameObject PlanetNameContainer;

    // account currencies
    public Text StarsAmount;
    public Text CreditsAmount;
    public Text CapsulesAmount;

    // planet resources
    public Sprite bg, teamBg;
    public GameObject PlanetImage;
    public Transform MetalPanel;
    public Transform MineralsPanel;
    public Transform GasPanel;
    public Transform EnergyPanel;
    public Transform ScrapPanel;
    public Transform HangarPanel;

    private GameObject ImageAndResouresContainer;
    private GameObject ImageContainer;
    private GameObject ResourcesContainer;
    private Planet currentPlanet;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void SetUpPlanet()
    {
        StartCoroutine(NetworkManager.instance.getTexture(currentPlanet.pictureId, SetPlanetImage));
        //ImageContainer.transform.Find("PlanetName").Find("Text").GetComponent<Text>().text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";
        PlanetNameContainer.transform.Find("Text").GetComponent<Text>().text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";
        SetResources();
        SetAccountResources();
        SetAttacks();
    }


    void test(JsonData response)
    {

    }


    void OnEnable()
    {
        Dictionary<string, string> ships = new Dictionary<string, string>();
        ships["1"] = "1";
        Dictionary<string, string> commanderShips = new Dictionary<string, string>();
        commanderShips["11"] = "1";
        string[] coordinates = { "1", "28", "1" };
        //NetworkManager.instance.CheckFleets(PlayerManager.instance.SessionToken, "sk", "1", "1", "attack", ships, commanderShips, "1", test);
        //NetworkManager.instance.GetFleets(PlayerManager.instance.SessionToken, "sk", "1", "1", test);
        //NetworkManager.instance.GetCommanderShips(PlayerManager.instance.SessionToken, "sk", "1", "1", test);
        //NetworkManager.instance.FleetsDetails(PlayerManager.instance.SessionToken, "sk", "1", "1", "338", test);
        //NetworkManager.instance.SendFleets(PlayerManager.instance.SessionToken, "sk", "1", "1", "attack", ships, commanderShips, "1", coordinates, test);


        //LocalizedText[] textFields = FindObjectsOfType<LocalizedText>();
        //foreach (LocalizedText text in textFields)
        //{
        //    text.LocalizeText();
        //}

        //currentPlanet = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID];

        //ImageAndResouresContainer = transform.Find("PlanetPanel").gameObject;
        //ImageContainer = ImageAndResouresContainer.transform.Find("PlanetImagePanel").gameObject;
        //ResourcesContainer = ImageAndResouresContainer.transform.Find("PlanetResourcesPanel").gameObject;
        //planetNameContainer.GetComponent<Image>().color = (currentPlanet.IsTeam == 0) ? new Color(27f / 255f, 119f / 255f, 166f / 255f) : new Color(163f / 255f, 98f / 255f, 10f / 255f);

        //SetUpPlanet();
        //InvokeRepeating("GetPlanetsInfo", 0.0f, 10.0f);


        LocalizedText[] textFields = FindObjectsOfType<LocalizedText>();
        foreach (LocalizedText text in textFields)
        {
            text.LocalizeText();
        }

        currentPlanet = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID];

        //ImageAndResouresContainer = transform.Find("PlanetPanel").gameObject;
        //ImageContainer = ImageAndResouresContainer.transform.Find("PlanetImagePanel").gameObject;
        //ResourcesContainer = ImageAndResouresContainer.transform.Find("PlanetResourcesPanel").gameObject;
        PlanetNameContainer.GetComponent<Image>().sprite = (currentPlanet.IsTeam == 0) ? bg : teamBg;

        SetUpPlanet();
        SetBg();
        InvokeRepeating("GetPlanetsInfo", 0.0f, 10.0f);
    }

    void OnDisable()
    {
        CancelInvoke("GetPlanetsInfo");
    }

    void GetPlanetsInfo()
    {
        NetworkManager.instance.getPlayerPlanets(PlayerManager.instance.SessionToken, "sk", "1", OnPlanetsInfo);
        NetworkManager.instance.getPlayerAttacks(PlayerManager.instance.SessionToken, "sk", "1", OnAttackInfo);
        NetworkManager.instance.GetCapsules(PlayerManager.instance.SessionToken, "sk", "1", OnCapsulesInfo);
    }

    // Planets Info
    private void OnPlanetsInfo(JsonData response)
    {
        PlayerManager.instance.planets = new Planet[response.Count];

        int i = 0;
        //foreach (JsonData elem in response as IList)
        //{
        //    PlayerManager.instance.planets[i] = new Planet(elem);
        //    i++;
        //}

        foreach (DictionaryEntry elem in response)
        {
            PlayerManager.instance.planets[i] = new Planet(elem.Value as JsonData);
            i++;
        }

        i = 0;

        currentPlanet = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID];

        // We refresh only the resources and attacks
        SetResources();
        SetPlanetBaseInfo(); // refreshes the image and coordinates
    }

    // Attacks Info
    public void OnAttackInfo(JsonData response)
    {
        PlayerManager.instance.attacksNumber = response.Count;
        SetAttacks();
        SetBg();
    }

    private void SetBg()
    {
        if (PlayerManager.instance.attacksNumber > 0)
        {
            GameManager.instance.ChangeBackground(GameManager.ATTACKED_BG);
        }
        else
        {
            GameManager.instance.ChangeBackground(GameManager.SAFE_BG);
        }
    }

    // Capsules Info
    public void OnCapsulesInfo(JsonData response)
    {
        int total = 0;
        for (int i = 0; i < response.Count; i++)
        {
            total += Int32.Parse(response[i].ToString());
        }
        PlayerManager.instance.Capsules = total.ToString();

        SetAccountResources(); // set capsules, stars, credits
    }

    void SetPlanetBaseInfo()
    {
        StartCoroutine(NetworkManager.instance.getTexture(currentPlanet.pictureId, SetPlanetImage));
        //ImageContainer.transform.Find("PlanetName").Find("Text").GetComponent<Text>().text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";
        PlanetNameContainer.transform.Find("Text").GetComponent<Text>().text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";

    }

    void SetResources()
    {
        SetMetal();
        SetMinerals();
        SetGas();
        SetEnergy();
        SetScrap();
        SetPopulation();
        SetPopulationUsed();
    }

    void SetAccountResources()
    {
        SetCredits();
        SetStars();
        SetCapsules();
    }

    void SetPlanetImage(Texture2D tex)
    {
        Rect rect = new Rect(0, 0, tex.width, tex.height);

        PlanetImage.GetComponent<Image>().sprite = Sprite.Create(tex, rect, new Vector2(0, 0));
    }

    void SetMetal()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Left").Find("MetalPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = currentPlanet.metal.ToString();
        //path.Find("ProgressBar").Find("MetalProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
        //    path.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.metalPercent,
        //    path.Find("ProgressBar").GetComponent<RectTransform>().rect.height);

        //if(currentPlanet.metalPercent >= 1)
        //{
        //    path.Find("ProgressBar").Find("MetalProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        //}
        //else
        //{
        //    path.Find("ProgressBar").Find("MetalProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        //}
        MetalPanel.Find("Amount").GetComponent<Text>().text = currentPlanet.metal.ToString();
        MetalPanel.Find("ProgressBar").Find("MetalProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
            MetalPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.metalPercent,
            MetalPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.height);
    }

    void SetMinerals()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Left").Find("MineralsPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = currentPlanet.crystal.ToString();
        //path.Find("ProgressBar").Find("MineralsProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
        //    path.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.crystalPercent,
        //    path.Find("ProgressBar").GetComponent<RectTransform>().rect.height);

        //if (currentPlanet.crystalPercent >= 1)
        //{
        //    path.Find("ProgressBar").Find("MineralsProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        //}
        //else
        //{
        //    path.Find("ProgressBar").Find("MineralsProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        //}

        MineralsPanel.Find("Amount").GetComponent<Text>().text = currentPlanet.crystal.ToString();
        MineralsPanel.Find("ProgressBar").Find("MineralsProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
            MineralsPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.crystalPercent,
            MineralsPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.height);

    }

    void SetGas()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Left").Find("GasPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = currentPlanet.gas.ToString();
        //path.Find("ProgressBar").Find("GasProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
        //    path.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.gasPercent,
        //    path.Find("ProgressBar").GetComponent<RectTransform>().rect.height);

        //if (currentPlanet.gasPercent >= 1)
        //{
        //    path.Find("ProgressBar").Find("GasProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        //}
        //else
        //{
        //    path.Find("ProgressBar").Find("GasProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        //}

        GasPanel.Find("Amount").GetComponent<Text>().text = currentPlanet.gas.ToString();
        GasPanel.Find("ProgressBar").Find("GasProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
            GasPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.gasPercent,
            GasPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.height);
    }

    void SetEnergy()
    {
        //ResourcesContainer.transform.Find("Panels").Find("Left").Find("EnergyPanel").Find("Panel").Find("Text").GetComponent<Text>().text = currentPlanet.energy.ToString();

        EnergyPanel.Find("Amount").GetComponent<Text>().text = currentPlanet.energy.ToString();
    }

    void SetScrap()
    {
        //ResourcesContainer.transform.Find("Panels").Find("Left").Find("ScrapPanel").Find("Panel").Find("Text").GetComponent<Text>().text = currentPlanet.recycle.ToString();

        ScrapPanel.Find("Amount").GetComponent<Text>().text = currentPlanet.recycle.ToString();
    }

    void SetPopulation()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Right").Find("PopulationPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = currentPlanet.popUsed.ToString();
        //path.Find("ProgressBar").Find("PopulationProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
        //   path.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.popUsedPercent,
        //   path.Find("ProgressBar").GetComponent<RectTransform>().rect.height);

        //if (currentPlanet.popUsedPercent >= 1)
        //{
        //    path.Find("ProgressBar").Find("PopulationProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        //}
        //else
        //{
        //    path.Find("ProgressBar").Find("PopulationProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        //}

        HangarPanel.Find("Amount").GetComponent<Text>().text = currentPlanet.popUsed.ToString();
        HangarPanel.Find("ProgressBar").Find("PopulationProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
           HangarPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.popUsedPercent,
           HangarPanel.Find("ProgressBar").GetComponent<RectTransform>().rect.height);
    }


    // TODO combine it with the population?
    void SetPopulationUsed()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Right").Find("PopulationUsedPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = currentPlanet.defPopUsed.ToString();
        //path.Find("ProgressBar").Find("PopulationUsedProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(
        //  path.Find("ProgressBar").GetComponent<RectTransform>().rect.width * (float)currentPlanet.defPopPercent,
        //  path.Find("ProgressBar").GetComponent<RectTransform>().rect.height);

        //if (currentPlanet.defPopPercent >= 1)
        //{
        //    path.Find("ProgressBar").Find("PopulationUsedProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        //}
        //else
        //{
        //    path.Find("ProgressBar").Find("PopulationUsedProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        //}
    }

    void SetCredits()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Right").Find("PremiumPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = PlayerManager.instance.Credits;

        CreditsAmount.text = PlayerManager.instance.Credits;
    }

    void SetStars()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Right").Find("PlatinumPanel").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = PlayerManager.instance.Stars;

        //Debug.Log(PlayerManager.instance.Stars);
        StarsAmount.text = PlayerManager.instance.Stars;
    }

    void SetCapsules()
    {
        //Transform path = ResourcesContainer.transform.Find("Panels").Find("Right").Find("Capsules").Find("Panel");
        //path.Find("Text").GetComponent<Text>().text = PlayerManager.instance.Capsules;

        CapsulesAmount.text = PlayerManager.instance.Capsules;
    }

    // TODO
    void SetAttacks()
    {
        return;
        //Attacks.text = "Incoming Attacks(" + PlayerManager.instance.attacksNumber + ")";

        Attacks.GetComponent<LocalizedText>().specialString = PlayerManager.instance.attacksNumber.ToString();
        Attacks.GetComponent<LocalizedText>().LocalizedID = "Inc_Attacks";
    }

    public void OnPlanetClicked()
    {
        GameManager.instance.ChangeView(GameManager.instance.planetView, GameManager.instance.gameView);
    }

    public void OnAttackClicked()
    {
        GameManager.instance.ChangeView(GameManager.instance.planetView, GameManager.instance.attackView);
    }

    public void OnBuildingsClicked()
    {
        GameManager.instance.actionType = C_ActionType.BUILDINGS;
        NavigateToActionView();
    }

    public void OnResearchesClicked()
    {
        GameManager.instance.actionType = C_ActionType.SCIENCES;
        NavigateToActionView();
    }

    public void OnUpgradesClicked()
    {
        GameManager.instance.actionType = C_ActionType.UPGRADES;
        NavigateToActionView();
    }

    public void OnShipsClicked()
    {
        GameManager.instance.actionType = C_ActionType.SHIPS;
        NavigateToActionView();
    }

    public void OnDefencesClicked()
    {
        GameManager.instance.actionType = C_ActionType.DEFENCES;
        NavigateToActionView();
    }

    private void NavigateToActionView()
    {
        GameManager.instance.ChangeView(GameManager.instance.CurrentView, GameManager.instance.actionView);
    }
}
