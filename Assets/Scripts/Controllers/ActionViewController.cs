﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LitJson;
using System.Linq;
using System;

public class ActionViewController : MonoBehaviour
{
    public Text planetName;
    public Image planetImage;
    public Sprite planetContainerImage;
    public Sprite planetContainerTeamImage;
    public GameObject planetNameContainer;
    public Text ozone;
    public Text metal;
    public Text crystal;
    public Text gas;
    public Text energy;
    public Text attacks;
    private Planet currentPlanet;

    public GameObject mainContainer;
    public int elementHeight;
    public int elementSpacing;

    //public GameObject label_1;
    //public GameObject label_2;
    //public GameObject label_3;
    //public GameObject label_4;

    public GameObject metalBar;
    public GameObject crystalBar;
    public GameObject gasBar;
    //public GameObject container_1;
    //public GameObject container_2;
    //public GameObject container_3;
    //public GameObject container_4;

    // Use for slow responses
    private bool sendBuildingsRequest;
    private bool shipsReady;
    private bool comShipsReady;

    void OnEnable()
    {
        currentPlanet = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID];
        int containers = 0;
        SetPlanetBaseInfo();
        //if (GameManager.instance.actionType == C_ActionType.BUILDINGS)
        //{
        //    container_1.tag = "Resources";
        //    container_2.tag = "Military";
        //    container_3.tag = "Alliance";

        //    containers = 3;

        //    label_1.SetActive(true);
        //    label_2.SetActive(true);
        //    label_3.SetActive(true);

        //    label_1.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Resources";
        //    label_2.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Military";
        //    label_3.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Alliance";

        //    sendBuildingsRequest = true;
        //}
        //else if(GameManager.instance.actionType == C_ActionType.UPGRADES)
        //{
        //    container_1.tag = "CivilianShips";
        //    container_2.tag = "WarShips";

        //    containers = 2;

        //    label_1.SetActive(true);
        //    label_2.SetActive(true);

        //    label_1.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "CivilianShips";
        //    label_2.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "WarShips";
        //}
        //else if(GameManager.instance.actionType == C_ActionType.SCIENCES)
        //{
        //    container_1.tag = "Basic";
        //    container_2.tag = "Advanced";
        //    container_3.tag = "Master";
        //    container_4.tag = "Additional";

        //    containers = 4;

        //    label_1.SetActive(true);
        //    label_2.SetActive(true);
        //    label_3.SetActive(true);
        //    label_4.SetActive(true);

        //    label_1.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Basic";
        //    label_2.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Advanced";
        //    label_3.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Master";
        //    label_4.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "Additional";
        //}
        //else if (GameManager.instance.actionType == C_ActionType.SHIPS)
        //{
        //    container_1.tag = "CivilianShips";
        //    container_2.tag = "WarShips";

        //    containers = 2;

        //    label_1.SetActive(true);
        //    label_2.SetActive(true);

        //    label_1.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "CivilianShips";
        //    label_2.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "WarShips";
        //}

        //else if (GameManager.instance.actionType == C_ActionType.DEFENCES)
        //{
        //    container_1.tag = "CivilianShips";
        //    container_2.tag = "WarShips";

        //    containers = 2;

        //    label_1.SetActive(true);
        //    label_2.SetActive(true);

        //    label_1.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "CivilianShips";
        //    label_2.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "WarShips";
        //}


        sendBuildingsRequest = false;
        shipsReady = false;
        comShipsReady = false;

        planetName.text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";
        SetResources();
        SetBars();
        SetBg();
        InvokeRepeating("GetInfo", 0f, 10f);

        //resourcePanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(mainContainer.GetComponent<RectTransform>().rect.width, 0);

        //mainContainer.GetComponent<ContainerResizer>().resizeContainer(containers*75);

        //planetNameContainer.GetComponent<Image>().color = (currentPlanet.IsTeam == 0) ? new Color(27f / 255f, 119f / 255f, 166f / 255f) : new Color(163f / 255f, 98f / 255f, 10f / 255f);
        planetNameContainer.GetComponent<Image>().sprite = (currentPlanet.IsTeam == 0) ? planetContainerImage : planetContainerTeamImage;


       

    }

    void OnDisable()
    {
        //foreach(Transform child in container_1.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}
        //foreach (Transform child in container_2.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}
        //foreach (Transform child in container_3.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}
        //foreach (Transform child in container_4.transform)
        //{
        //    GameObject.Destroy(child.gameObject);
        //}

        foreach (Transform child in mainContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        //mainContainer.GetComponent<ContainerResizer>().resizeContainer(225);
        CancelInvoke("GetInfo");
        //SetButtonsInactive();
        CleanUp();
    }
    
     private void SetBg()
    {
        if (PlayerManager.instance.attacksNumber > 0)
        {
            GameManager.instance.ChangeBackground(GameManager.ATTACKED_BG);
        }
        else
        {
            GameManager.instance.ChangeBackground(GameManager.SAFE_BG);
        }
    }

    void GetInfo()
    {
        if (GameManager.instance.actionType == C_ActionType.BUILDINGS)
        {
            
            if (!sendBuildingsRequest)
            {
                Debug.Log("buildings");
                NetworkManager.instance.getBuildings(PlayerManager.instance.SessionToken, "sk", "1", currentPlanet.id, OnBuildingsInfo);

                sendBuildingsRequest = true; // some problems when set to false
            }

        }
        else if (GameManager.instance.actionType == C_ActionType.UPGRADES)
        {
            NetworkManager.instance.GetUpgrades(PlayerManager.instance.SessionToken, "sk", "1", currentPlanet.id, OnUpgradesInfo);
        }
        else if (GameManager.instance.actionType == C_ActionType.SCIENCES)
        {
            NetworkManager.instance.GetSciences(PlayerManager.instance.SessionToken, "sk", "1", currentPlanet.id, OnSciencesInfo);
        }
        else if (GameManager.instance.actionType == C_ActionType.SHIPS)
        {
            NetworkManager.instance.GetShips(PlayerManager.instance.SessionToken, "sk", "1", currentPlanet.id, OnShipsInfo);
            NetworkManager.instance.GetCommanderShips(PlayerManager.instance.SessionToken, currentPlanet.id, OnCommanderShipsInfo);
            if (comShipsReady)
            {
                NetworkManager.instance.CommanderShipUpgradeFetch(PlayerManager.instance.SessionToken, OnComShipsUpgInfo);
            }
        }
        else if (GameManager.instance.actionType == C_ActionType.DEFENCES)
        {
            NetworkManager.instance.GetDefence(PlayerManager.instance.SessionToken, "sk", "1", currentPlanet.id, OnDefencesInfo);
        }

        NetworkManager.instance.getPlayerAttacks(PlayerManager.instance.SessionToken, "sk", "1", OnAttackInfo);
        NetworkManager.instance.getPlayerPlanets(PlayerManager.instance.SessionToken, "sk", "1", OnPlanetsInfo);
    }

    void OnPlanetsInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView)
        {
            return;
        }

        PlayerManager.instance.planets = new Planet[response.Count];

        int i = 0;
        //foreach (JsonData elem in response as IList)
        //{
        //    PlayerManager.instance.planets[i] = new Planet(elem);
        //    i++;
        //}
        foreach (DictionaryEntry elem in response)
        {
            PlayerManager.instance.planets[i] = new Planet(elem.Value as JsonData);
            i++;
        }
        i = 0;

        currentPlanet = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID];

        SetResources();
        SetBars();
        SetBarColors();
    }

    void SetResources()
    {
        ozone.text = currentPlanet.ozone.ToString();
        metal.text = currentPlanet.metal.ToString();
        crystal.text = currentPlanet.crystal.ToString();
        gas.text = currentPlanet.gas.ToString();
        energy.text = currentPlanet.energy.ToString();
    }

    void SetBars()
    {
        metalBar.transform.Find("MetalProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(metalBar.GetComponent<RectTransform>().rect.width * 
            (float)currentPlanet.metalPercent, metalBar.GetComponent<RectTransform>().rect.height);
        crystalBar.transform.Find("MineralsProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(crystalBar.GetComponent<RectTransform>().rect.width * 
            (float)currentPlanet.crystalPercent, crystalBar.GetComponent<RectTransform>().rect.height);
        gasBar.transform.Find("GasProgressBar").GetComponent<RectTransform>().sizeDelta = new Vector2(gasBar.GetComponent<RectTransform>().rect.width * 
            (float)currentPlanet.gasPercent, gasBar.GetComponent<RectTransform>().rect.height);
    }

    void SetBarColors()
    {
        if (currentPlanet.metalPercent >= 1)
        {
            metalBar.transform.Find("MetalProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        }
        else
        {
            metalBar.transform.Find("MetalProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        }

        if (currentPlanet.crystalPercent >= 1)
        {
            crystalBar.transform.Find("MineralsProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        }
        else
        {
            crystalBar.transform.Find("MineralsProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        }

        if (currentPlanet.gasPercent >= 1)
        {
            gasBar.transform.Find("GasProgressBar").GetComponent<Image>().color = new Color(1, 1, 0);
        }
        else
        {
            gasBar.transform.Find("GasProgressBar").GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    
    void OnAttackInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView)
        {
            return;
        }
        PlayerManager.instance.attacksNumber = response.Count;
        SetAttacks();
        SetBg();
    }

    void SetAttacks()
    {
        ///TODO FIX
        //attacks.GetComponent<LocalizedText>().specialString = PlayerManager.instance.attacksNumber.ToString();
        //attacks.GetComponent<LocalizedText>().LocalizedID = "Inc_Attacks";
    }

    void OnBuildingsInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.BUILDINGS)
        {
            return;
        }
        sendBuildingsRequest = false;

        PlayerManager.instance.buildings = new Building[response.Count - 1];
        int i = 0;
        foreach (DictionaryEntry elem in response)
        {
            //Debug.Log((elem.Value as JsonData)["building_id"].ToString());
            if ((elem.Value as JsonData)["building_id"].ToString() != "18")
            {
                PlayerManager.instance.buildings[i] = new Building(elem.Value as JsonData);
                i++;
            }


            //Debug.Log(PlayerManager.instance.buildings[i].Type + " " + PlayerManager.instance.buildings[i].Id);

        }

        PlayerManager.instance.buildings = PlayerManager.instance.buildings.OrderBy(item => item.Id).ToArray();
        NetworkManager.instance.GetProcessQueue(PlayerManager.instance.SessionToken, "sk", "1", "buildings", OnQueueInfo);
        CreateBuilding();


    }

    private void CreateBuilding()
    {
        int buildingsCreated = GameObject.FindGameObjectsWithTag(C_ActionType.BUILDINGS).Length;

        if (buildingsCreated < PlayerManager.instance.buildings.Length)
        {
            for (int i = 0; i < PlayerManager.instance.buildings.Length; i++)
            {
                GameObject building = Instantiate(Resources.Load("Prefabs/BuildingsPrefab")) as GameObject;
                building.tag = "Buildings";
                building.GetComponent<CreateActionElement>().id = PlayerManager.instance.buildings[i].Id;
                building.GetComponent<CreateActionElement>().SetElement();
            }
        }

        SetContainerSize(PlayerManager.instance.buildings.Length);
        //SetButtonsActive();
    }

    private void OnUpgradesInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.UPGRADES)
        {
            return;
        }
        PlayerManager.instance.upgrades = new Upgrade[response.Count];
        int i = 0;


        foreach (JsonData elem in response as IList)
        {
            //Debug.Log(elem["ship_id"].ToString());
            PlayerManager.instance.upgrades[i] = new Upgrade(elem as JsonData);
            i++;

        }

        PlayerManager.instance.upgrades = PlayerManager.instance.upgrades.OrderBy(item => item.Id).ToArray();
        NetworkManager.instance.GetProcessQueue(PlayerManager.instance.SessionToken, "sk", "1", "shipsupgrades", OnQueueInfo);
        CreateUpgrade();

    }

    private void CreateUpgrade()
    {
        int upgradesCreated = GameObject.FindGameObjectsWithTag(C_ActionType.UPGRADES).Length;

        if (upgradesCreated < PlayerManager.instance.upgrades.Length)
        {
            for (int i = 0; i < PlayerManager.instance.upgrades.Length; i++)
            {
                GameObject upgrade = Instantiate(Resources.Load("Prefabs/BuildingsPrefab")) as GameObject;
                upgrade.tag = "Upgrades";
                upgrade.GetComponent<CreateActionElement>().id = PlayerManager.instance.upgrades[i].Id;
                upgrade.GetComponent<CreateActionElement>().SetElement();
            }
        }

        SetContainerSize(PlayerManager.instance.upgrades.Length);
        //SetButtonsActive();
    }

    private void OnSciencesInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.SCIENCES)
        {
            return;
        }
        JsonData sciencesResponse = response[0];
        PlayerManager.instance.sciences = new Science[sciencesResponse.Count];
        int i = 0;


        //foreach (JsonData elem in sciencesResponse as IList)
        //{
        //    //Debug.Log(elem["ship_id"].ToString());
        //    PlayerManager.instance.sciences[i] = new Science(elem as JsonData);
        //    i++;

        //}

        foreach (DictionaryEntry elem in sciencesResponse)
        {

            PlayerManager.instance.sciences[i] = new Science(elem.Value as JsonData);
            i++;
        }

        PlayerManager.instance.sciences = PlayerManager.instance.sciences.OrderBy(item => item.Id).ToArray();
        NetworkManager.instance.GetProcessQueue(PlayerManager.instance.SessionToken, "sk", "1", "sciences", OnQueueInfo);
        CreateScience();

    }

    private void CreateScience()
    {
        int sciencesCreated = GameObject.FindGameObjectsWithTag(C_ActionType.SCIENCES).Length;

        if (sciencesCreated < PlayerManager.instance.sciences.Length)
        {
            for (int i = 0; i < PlayerManager.instance.sciences.Length; i++)
            {
                GameObject science = Instantiate(Resources.Load("Prefabs/BuildingsPrefab")) as GameObject;
                science.tag = "Sciences";
                science.GetComponent<CreateActionElement>().id = PlayerManager.instance.sciences[i].Id;
                science.GetComponent<CreateActionElement>().SetElement();
            }
        }

        SetContainerSize(PlayerManager.instance.sciences.Length);
        //SetButtonsActive();
    }

    private void OnShipsInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.SHIPS)
        {
            return;
        }
        //if (PlayerManager.instance.ships != null)
        //{
        //    Debug.Log(PlayerManager.instance.ships[0].QueueId);
        //}
        JsonData shipsResponse = response[0];
        PlayerManager.instance.ships = new Ship[shipsResponse.Count];
        int i = 0;


        foreach (DictionaryEntry elem in shipsResponse)
        {
            //Debug.Log(elem["ship_id"].ToString());


            PlayerManager.instance.ships[i] = new Ship(elem.Value as JsonData);

            i++;

        }

        PlayerManager.instance.ships = PlayerManager.instance.ships.OrderBy(item => item.Id).ToArray();
        NetworkManager.instance.GetProcessQueue(PlayerManager.instance.SessionToken, "sk", "1", "ships", OnQueueInfo);
        shipsReady = true;

        if(comShipsReady)
        {
            CreateShips();
        }
    }

    private void OnCommanderShipsInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.SHIPS)
        {
            return;
        }

        JsonData comShipsResponse = response[0];
        if (PlayerManager.instance.comShips == null)
        {
            PlayerManager.instance.comShips = new CommanderShip[comShipsResponse.Count];
        }

        int i = 0;

        foreach (DictionaryEntry elem in comShipsResponse)
        {
            if (PlayerManager.instance.comShips[Int32.Parse(elem.Key.ToString()) - 1] != null)
            {
                PlayerManager.instance.comShips[Int32.Parse(elem.Key.ToString()) - 1].SetComShipInfo(elem.Value as JsonData);
            }
            else
            {
                PlayerManager.instance.comShips[Int32.Parse(elem.Key.ToString()) - 1] = new CommanderShip(elem.Value as JsonData);
            }

            i++;
        }

        PlayerManager.instance.comShips = PlayerManager.instance.comShips.OrderBy(item => item.Id).ToArray();
        ///TODO QUEUE

        if (!comShipsReady)
        {
            NetworkManager.instance.CommanderShipUpgradeFetch(PlayerManager.instance.SessionToken, OnComShipsUpgInfo);
        }
        

        comShipsReady = true;

        if(shipsReady)
        {
            CreateShips();
        }
    }

    private void OnComShipsUpgInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.SHIPS)
        {
            return;
        }
        JsonData comUpgResponse = response[0];
        
        foreach (DictionaryEntry elem in comUpgResponse)
        {
            PlayerManager.instance.comShips[Int32.Parse(elem.Key.ToString()) - 1].SetUpgInfo(elem.Value as JsonData);
        }

        if(shipsReady && comShipsReady)
        {
            CreateShips();
        }
    }


    // creates ships/com ships OR updates the current one
    private void CreateShips()
    {
        int shipsCreated = GameObject.FindGameObjectsWithTag(C_ActionType.SHIPS).Length;

        if (shipsCreated < PlayerManager.instance.ships.Length)
        {
            for (int i = 0; i < PlayerManager.instance.ships.Length; i++)
            {
                GameObject ship = Instantiate(Resources.Load("Prefabs/BuildingsPrefab")) as GameObject;
                ship.tag = "Ships";
                ship.GetComponent<CreateActionElement>().id = PlayerManager.instance.ships[i].Id;
                ship.GetComponent<CreateActionElement>().SetElement();
            }
        }

        int comShipsCreated = GameObject.FindGameObjectsWithTag(C_ActionType.COMMANDER_SHIPS).Length;

        
        for (int i = 0; i < PlayerManager.instance.comShips.Length; i++)
        {
            if (comShipsCreated < PlayerManager.instance.comShips.Length)
            {
                GameObject comShip = Instantiate(Resources.Load("Prefabs/ComShipsPrefab")) as GameObject;
                comShip.tag = C_ActionType.COMMANDER_SHIPS;
                comShip.GetComponent<CreateComShipElement>().id = PlayerManager.instance.comShips[i].Id;
                comShip.GetComponent<CreateComShipElement>().comShip = PlayerManager.instance.comShips[i];
                comShip.GetComponent<CreateComShipElement>().SetElement();
            }
            else
            {
                GameObject comShip = GameObject.FindGameObjectWithTag("ScrollContainer").transform.GetChild(i + PlayerManager.instance.ships.Length).gameObject;
                comShip.GetComponent<CreateComShipElement>().comShip = PlayerManager.instance.comShips[i];
                comShip.GetComponent<CreateComShipElement>().SetElement();
            }
        }
        

        SetContainerSize(PlayerManager.instance.ships.Length + PlayerManager.instance.comShips.Length);
        //SetButtonsActive();
    }

    public void OnComShipTrain(JsonData response)
    {
        string responseType = GameManager.instance.HandleResponse(response);
        if (responseType == GameManager.SUCCESS)
        {
            if ((bool)response["success"] == true)
            {
                NetworkManager.instance.GetCommanderShips(PlayerManager.instance.SessionToken, currentPlanet.id, OnCommanderShipsInfo);
                NetworkManager.instance.CommanderShipUpgradeFetch(PlayerManager.instance.SessionToken, OnComShipsUpgInfo);
            }
        }
        else if (responseType == GameManager.ERROR)
        {
            ActionViewPopUp errorPopup = new ActionViewPopUp();
            errorPopup.DescriptionText = "ERROR!\n" + response["message"].ToString();
            errorPopup.Create();
        }
    }

    public void OnComShipUpgrade(JsonData response)
    {
        string responseType = GameManager.instance.HandleResponse(response);
        if (responseType == GameManager.SUCCESS)
        {
            if ((bool)response["success"] == true)
            {
                NetworkManager.instance.GetCommanderShips(PlayerManager.instance.SessionToken, currentPlanet.id, OnCommanderShipsInfo);
                NetworkManager.instance.CommanderShipUpgradeFetch(PlayerManager.instance.SessionToken, OnComShipsUpgInfo);
            }
        }
        else if (responseType == GameManager.ERROR)
        {
            ActionViewPopUp errorPopup = new ActionViewPopUp();
            errorPopup.DescriptionText = "ERROR!\n" + response["message"].ToString();
            errorPopup.Create();
        }
    }

    private void OnDefencesInfo(JsonData response)
    {
        if (GameManager.instance.CurrentView != GameManager.instance.actionView || GameManager.instance.actionType != C_ActionType.DEFENCES)
        {
            return;
        }
        JsonData defencesResponse = response[0];
        PlayerManager.instance.defences = new Defence[defencesResponse.Count];
        int i = 0;


        foreach (DictionaryEntry elem in defencesResponse)
        {
            //Debug.Log(elem["ship_id"].ToString());
            PlayerManager.instance.defences[i] = new Defence(elem.Value as JsonData);

            i++;

        }

        PlayerManager.instance.defences = PlayerManager.instance.defences.OrderBy(item => item.Id).ToArray();
        NetworkManager.instance.GetProcessQueue(PlayerManager.instance.SessionToken, "sk", "1", "defence", OnQueueInfo);
        CreateDefences();
    }

    private void CreateDefences()
    {
        int defencesCreated = GameObject.FindGameObjectsWithTag(C_ActionType.DEFENCES).Length;

        if (defencesCreated < PlayerManager.instance.defences.Length)
        {
            for (int i = 0; i < PlayerManager.instance.defences.Length; i++)
            {
                GameObject defence = Instantiate(Resources.Load("Prefabs/BuildingsPrefab")) as GameObject;
                defence.tag = "Defences";
                defence.GetComponent<CreateActionElement>().id = PlayerManager.instance.defences[i].Id;
                defence.GetComponent<CreateActionElement>().SetElement();
            }
        }

        SetContainerSize(PlayerManager.instance.defences.Length);
        //SetButtonsActive();
    }

    // Queue
    private void OnQueueInfo(JsonData response)
    {
        if ((bool)response["success"] == false || response["processes"] == null)
        {
            return;
        }
        else
        {
            JsonData data = response["processes"] as JsonData;
            //Debug.Log(data["id"]);
            if (GameManager.instance.actionType == C_ActionType.BUILDINGS)
            {
                foreach (Building bld in PlayerManager.instance.buildings)
                {
                    foreach (JsonData element in data as IList)
                    {
                        if (bld.Id == Int32.Parse(element["building_id"].ToString()) && currentPlanet.id == element["planet_id"].ToString())
                        {
                            bld.SetQueueId(element["id"].ToString());
                        }
                    }
                }
            }

            else if (GameManager.instance.actionType == C_ActionType.SCIENCES)
            {
                foreach (Science scn in PlayerManager.instance.sciences)
                {
                    foreach (JsonData element in data as IList)
                    {
                        if (scn.Id == Int32.Parse(element["science_id"].ToString()))
                        {
                            scn.SetQueueId(element["id"].ToString());
                        }
                    }
                }
            }

            else if (GameManager.instance.actionType == C_ActionType.UPGRADES)
            {
                foreach (Upgrade upg in PlayerManager.instance.upgrades)
                {
                    foreach (JsonData element in data as IList)
                    {
                        if (upg.Id == Int32.Parse(element["ship_id"].ToString()))
                        {
                            upg.SetQueueId(element["id"].ToString());
                        }
                    }
                }
            }

            else if (GameManager.instance.actionType == C_ActionType.SHIPS)
            {
                foreach (Ship ship in PlayerManager.instance.ships)
                {
                    ship.SetQueueId(null);

                    foreach (JsonData element in data as IList)
                    {
                        if (ship.Id == Int32.Parse(element["ship_id"].ToString()))
                        {
                            ship.SetQueueId(element["id"].ToString());
                        }
                    }
                }
            }

            else if (GameManager.instance.actionType == C_ActionType.DEFENCES)
            {
                foreach (Defence defence in PlayerManager.instance.defences)
                {
                    defence.SetQueueId(null);

                    foreach (JsonData element in data as IList)
                    {
                        if (defence.Id == Int32.Parse(element["defence_id"].ToString()))
                        {
                            defence.SetQueueId(element["id"].ToString());
                        }
                    }
                }
            }
        }
    }

    private void SetContainerSize(int childNum)
    {
        mainContainer.GetComponent<GridLayoutGroup>().cellSize = new Vector2(mainContainer.GetComponent<GridLayoutGroup>().cellSize.x, elementHeight);
        mainContainer.GetComponent<GridLayoutGroup>().spacing = new Vector2(mainContainer.GetComponent<GridLayoutGroup>().cellSize.x, elementSpacing);
        mainContainer.GetComponent<ContainerResizer>().resizeContainer(childNum * elementHeight + (childNum - 1) * elementSpacing);
    }

    //public void OnPanel1Click()
    //{
    //    float container_1_ChildCount = container_1.transform.childCount;
    //    if (container_1.GetComponent<GridLayoutGroup>().cellSize.y == 75)
    //    {
    //        container_1.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_1.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        container_1.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_1.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_1_ChildCount * 80f));
    //    }
    //    else
    //    {
    //        container_1.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_1.GetComponent<GridLayoutGroup>().cellSize.x, 75);
    //        container_1.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_1.GetComponent<GridLayoutGroup>().cellSize.x, 5);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height + (container_1_ChildCount * 80f));
    //        //industryPanel.GetComponent<LayoutElement>().preferredHeight = 0;
    //        //militaryPanel.GetComponent<LayoutElement>().preferredHeight = 0;

    //    }
    //    //StartCoroutine(SetResourceContainer());
    //}

    //public void OnPanel2Click()
    //{
    //    float container_2_ChildCount = container_2.transform.childCount;
    //    if (container_2.GetComponent<GridLayoutGroup>().cellSize.y == 75)
    //    {
    //        container_2.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_2.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        container_2.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_2.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_2_ChildCount * 80f));
    //    }
    //    else
    //    {
    //        container_2.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_2.GetComponent<GridLayoutGroup>().cellSize.x, 75);
    //        container_2.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_2.GetComponent<GridLayoutGroup>().cellSize.x, 5);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height + (container_2_ChildCount * 80f));
    //        //resourcePanel.GetComponent<LayoutElement>().preferredHeight = 0;
    //        //industryPanel.GetComponent<LayoutElement>().preferredHeight = 445;
    //        //militaryPanel.GetComponent<LayoutElement>().preferredHeight = 0;

    //    }

    //    //StartCoroutine(SetIndustryContainer());

    //}

    //public void OnPanel3Click()
    //{
    //    float container_3_ChildCount = container_3.transform.childCount;
    //    if (container_3.GetComponent<GridLayoutGroup>().cellSize.y == 75)
    //    {
    //        container_3.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_3.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        container_3.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_3.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_3_ChildCount * 80f));
    //    }
    //    else
    //    {
    //        container_3.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_3.GetComponent<GridLayoutGroup>().cellSize.x, 75);
    //        container_3.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_3.GetComponent<GridLayoutGroup>().cellSize.x, 5);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height + (container_3_ChildCount * 80f));
    //        //resourcePanel.GetComponent<LayoutElement>().preferredHeight = 0;
    //        //industryPanel.GetComponent<LayoutElement>().preferredHeight = 0;
    //        //militaryPanel.GetComponent<LayoutElement>().preferredHeight = 445;

    //    }
    //    //StartCoroutine(SetMilitaryContainer());
    //}

    //public void OnPanel4Click()
    //{
    //    float container_4_ChildCount = container_4.transform.childCount;
    //    if (container_4.GetComponent<GridLayoutGroup>().cellSize.y == 75)
    //    {
    //        container_4.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_4.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        container_4.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_4.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_4_ChildCount * 80f));
    //    }
    //    else
    //    {
    //        container_4.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_4.GetComponent<GridLayoutGroup>().cellSize.x, 75);
    //        container_4.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_4.GetComponent<GridLayoutGroup>().cellSize.x, 5);
    //        mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height + (container_4_ChildCount * 80f));
    //    }
    //}


    //private void CloseAll()
    //{
    //    container_1.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_1.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    container_1.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_1.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_2.transform.childCount * 80f));

    //    container_3.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_3.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    container_3.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_3.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_3.transform.childCount * 80f));

    //    container_2.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_2.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    container_2.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_2.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_2.transform.childCount * 80f));

    //    container_4.GetComponent<GridLayoutGroup>().cellSize = new Vector2(container_4.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    container_4.GetComponent<GridLayoutGroup>().spacing = new Vector2(container_4.GetComponent<GridLayoutGroup>().cellSize.x, 0);
    //    mainContainer.GetComponent<ContainerResizer>().resizeContainer(mainContainer.GetComponent<RectTransform>().rect.height - (container_4.transform.childCount * 80f));

    //}
    public void OnBackButtonClick()
    {
        //CloseAll();
        mainContainer.GetComponent<RectTransform>().position = new Vector2(mainContainer.GetComponent<RectTransform>().position.x, 0);
        mainContainer.GetComponent<ContainerResizer>().resizeContainer(0);
        GameManager.instance.ChangeView(GameManager.instance.CurrentView, GameManager.instance.planetView);
    }

    //public void SetButtonsActive()
    //{
    //    label_1.GetComponent<Button>().interactable = true;
    //    label_2.GetComponent<Button>().interactable = true;
    //    label_3.GetComponent<Button>().interactable = true;
    //    label_4.GetComponent<Button>().interactable = true;
    //}

    //public void SetButtonsInactive()
    //{
    //    label_1.GetComponent<Button>().interactable = false;
    //    label_2.GetComponent<Button>().interactable = false;
    //    label_3.GetComponent<Button>().interactable = false;
    //    label_4.GetComponent<Button>().interactable = false;
    //}

    public void CleanUp()
    {
        //container_1.tag = "Untagged";
        //container_2.tag = "Untagged";
        //container_3.tag = "Untagged";
        //container_4.tag = "Untagged";

        //label_1.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "";
        //label_2.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "";
        //label_3.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "";
        //label_4.transform.Find("Text").GetComponent<LocalizedText>().LocalizedID = "";

        //label_1.SetActive(false);
        //label_2.SetActive(false);
        //label_3.SetActive(false);
        //label_4.SetActive(false);

        PlayerManager.instance.buildings = null;
        PlayerManager.instance.upgrades = null;
        PlayerManager.instance.sciences = null;
        PlayerManager.instance.ships = null;
        PlayerManager.instance.comShips = null;
        PlayerManager.instance.defences = null;
    }

    void SetPlanetBaseInfo()
    {
        StartCoroutine(NetworkManager.instance.getTexture(currentPlanet.pictureId, SetPlanetImage));
        //ImageContainer.transform.Find("PlanetName").Find("Text").GetComponent<Text>().text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";
        planetNameContainer.transform.Find("Text").GetComponent<Text>().text = currentPlanet.name + "\n(" + currentPlanet.coordinates.x + ":" + currentPlanet.coordinates.y + ":" + currentPlanet.coordinates.z + ")";

    }

    void SetPlanetImage(Texture2D tex)
    {
        Rect rect = new Rect(0, 0, tex.width, tex.height);

        planetImage.GetComponent<Image>().sprite = Sprite.Create(tex, rect, new Vector2(0, 0));
    }
}
