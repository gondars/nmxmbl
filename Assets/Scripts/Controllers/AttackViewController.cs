﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Linq;

public class AttackViewController : MonoBehaviour
{

    private string sessionToken;
    private string gameId;
    private string server;
    private string planetId;
    public string attackType;
    private Opponent[] opponents;

    void OnEnable()
    {
        sessionToken = PlayerManager.instance.SessionToken;
        gameId = NetworkManager.instance.gameId;
        server = NetworkManager.instance.server;
        planetId = PlayerManager.instance.planets[GameManager.instance.chosenPlanetID].id;
        GetInfo();
        InvokeRepeating("GetFleetsInfo", 0.0f, 10.0f);
    }

    void OnDisable()
    {
        CancelInvoke("GetFleetsInfo");
    }

    void GetInfo()
    {
        //TODO Get ships, get commanderShips, get Enemies
        NetworkManager.instance.GetShips(sessionToken, gameId, server, planetId, OnShipsInfo);
        NetworkManager.instance.GetCommanderShips(sessionToken, planetId, OnCommanderShipsInfo);
        NetworkManager.instance.GetOpponents(sessionToken, gameId, server, OnOpponentsInfo);
    }

    private void GetFleetsInfo()
    {

    }

    private void OnShipsInfo(JsonData response)
    {
        JsonData shipsResponse = response[0];
        PlayerManager.instance.ships = new Ship[shipsResponse.Count];
        int i = 0;

        foreach (DictionaryEntry elem in shipsResponse)
        {
            PlayerManager.instance.ships[i] = new Ship(elem.Value as JsonData);
            i++;
        }

        PlayerManager.instance.ships = PlayerManager.instance.ships.OrderBy(item => item.Id).ToArray();

        // TODO set visualization
    }

    private void OnCommanderShipsInfo(JsonData response)
    {
        JsonData comShipsResponse = response[0];
        PlayerManager.instance.comShips = new CommanderShip[comShipsResponse.Count];

        int i = 0;
        foreach (DictionaryEntry elem in comShipsResponse)
        {
            PlayerManager.instance.comShips[i] = new CommanderShip(elem.Value as JsonData);
            i++;
        }
    }

    private void OnOpponentsInfo(JsonData response)
    {
        if (response.Keys.Contains("error"))
        {
            return;
        }
        JsonData results = response["results"];
        opponents = new Opponent[results.Count];

        for (int i = 0; i < opponents.Length; i++)
        {
            opponents[i] = new Opponent(results[i]);
        }
    }

    private void OnFleetsInfo(JsonData response)
    {

    }

    public void SetAttackType(string type)
    {
        attackType = type;
    }

    public void CheckFleet()
    {
        // TODO on success call SendFleet
        //TODO Finish passing ships and commanderships
        //NetworkManager.instance.CheckFleets(sessionToken, gameId, server, planetId, attackType, )
    }

    public void SendFleet()
    {

    }

    public void ReturnFleet()
    {

    }

    public void SetScrapMotivation()
    {

    }

    public void SetSpeedMotivation()
    {

    }

}
