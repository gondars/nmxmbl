﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Text playerName;
    public Text currLangUI;
    public int currentLanguageId = 0;
    public string currentLanguageText;
    public LocalizedText currentLanguage;

    void Awake()
    {
    }

    void Start()
    {

    }

    void OnEnable()
    {
        for (int i = 0; i < LanguageManager.Instance.languages.Count; i++)
        {
            if (LanguageManager.Instance.languages[i] == LanguageManager.Instance.currentLocale)
            {
                currentLanguageId = i;
            }
        }

        SetInfo();
    }

    private void SetInfo()
    {
        currentLanguageText = LanguageManager.Instance.languages[currentLanguageId];
        currLangUI.text = currentLanguageText;
        currentLanguage.LocalizedID = currentLanguageText;

        playerName.text = PlayerManager.instance.PlayerName;
    }

    public void LogOut()
    {
        if (GameManager.instance.CurrentView)
        {
            PlayerManager.instance.SessionToken = "";
            LocalDataManager.instance.SaveData("rememberMe", "false");
            LocalDataManager.instance.SaveData("sessionToken", null);
            GameManager.instance.ChangeView(GameManager.instance.CurrentView, GameManager.instance.loginView);
            //GameManager.instance.CurrentView = null;
        }
        gameObject.SetActive(false);
    }

    public void OnLeftLanguageButton()
    {
        if (currentLanguageId != 0)
        {
            currentLanguageId--;

        }
        else
        {
            currentLanguageId = LanguageManager.Instance.languages.Count - 1;
        }

        currentLanguageText = LanguageManager.Instance.languages[currentLanguageId];
        currLangUI.text = currentLanguageText;

        ChangeLanguage();

        currentLanguage.LocalizedID = currentLanguageText;
    }

    public void OnRightLanguageButton()
    {
        if (currentLanguageId != LanguageManager.Instance.languages.Count - 1)
        {
            currentLanguageId++;

        }
        else
        {
            currentLanguageId = 0;
        }

        currentLanguageText = LanguageManager.Instance.languages[currentLanguageId];
        currLangUI.text = currentLanguageText;
        //LanguageManager.Instance.currentLocale = currentLanguageText;

        ChangeLanguage();

        currentLanguage.LocalizedID = currentLanguageText;
    }

    void ChangeLanguage()
    {
        LanguageManager.Instance.LoadLanguage(LanguageManager.Instance.languages[currentLanguageId]);
    }
}
