﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string localizedID = string.Empty;
    public List<string> localizedIDs;
    public string specialString = string.Empty;

    public string LocalizedID
    {
        set
        {
            localizedID = value;
            localizedIDs = new List<string>();
            LocalizeText();
        }
    }

    public List<string> LocalizedIDs
    {
        set
        {
            localizedIDs = value;
            localizedID = string.Empty;
            LocalizeText();
        }
    }

    void Start()
    {
        LocalizeText();
    }

    public void LocalizeText()
    {
        Text text = GetComponent<Text>();
        text.text = "";
        if (text != null)
        {
            if (localizedIDs.Count > 0)
            {
                for (int i = 0; i < localizedIDs.Count; i++)
                {
                    if (LanguageManager.Instance.CheckIfSpecial(localizedIDs[i]))
                    {
                        text.text += LanguageManager.Instance.GetString(localizedIDs[i], localizedIDs[++i]);
                    }
                    else
                    {
                        text.text += LanguageManager.Instance.GetString(localizedIDs[i]);
                    }
                }
            }
            else
            {
                text.text = LanguageManager.Instance.GetString(localizedID, specialString);
            }
        }
    }
}
