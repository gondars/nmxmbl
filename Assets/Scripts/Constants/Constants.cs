﻿using UnityEngine;
using System.Collections;

public static class Constants
{
    /// <summary>
    /// TAGS
    /// </summary>
    ///

    // views
    public const string LOGIN_VIEW = "LoginView";
    public const string GAME_VIEW = "GameView";

    // other constants
    public const string TAG_ADVISERTEXT = "AdviserText";
    public const string TAG_NEWS = "NewsText";
    public const string PLANET_CONTAINER = "PlanetsContainer";
    public const string MESSAGE_INFO = "MessageInfo";

    public const string ADV_HELLO = "Hello_Commander";
    public const string ADV_SORRY = "Sorry_Msg";
    public const string ADV_ERROR = "Invalid_Pass_Msg";
    public const string ADV_ERROR_1 = "No_Such_User_Msg";
    public const string ADV_ERROR_TAKEN = "Taken_Email_Msg";
    //public const string ADV_HELLO = "Hello, Commander!\n\nAre you ready to join the endless adventure? Don't waste anymore time.\n\nEnter the amazing universe of Nemexia and test your warfare skills against thousands of players. Who know, maybe you will find a friend or two too.";
    public const string ADV_REGISTER = "Commander,\n\nPlease enter an account name, password and E-Mail to register your account";
    //public const string ADV_ERROR = "I am sorry, Commander\n\nI couldn't find you in the universe.\nDo you want to register an account? ";
    //public const string ADV_ERROR_TAKEN = "I am sorry, Commander\n\nThis username (email) is already taken. Try another.";

    // messages constants
    public const string M_SEND = "0";
    public const string M_PERSONAL = "1";
    public const string M_ADMINISTRATIVE = "2";
    public const string M_REPORTS = "3";
    public const string M_FLEETS = "4";
    public const string M_ALLIANCE = "5";
    public const string M_WALL = "6";
    public const string M_PAYMENTS = "7";
    public const string M_FEEDBACK = "8";
    public const string M_ACHIEVEMENTS = "9";
    public const string M_ARENA_REPORTS = "10";
    public const string M_TEAM_REPORTS = "11";
}
