﻿using UnityEngine;
using System.Collections;

public static class C_ActionType 
{
    public static string BUILDINGS = "Buildings";
    public static string SHIPS = "Ships";
    public static string COMMANDER_SHIPS = "CommanderShips";
    public static string DEFENCES = "Defences";
    public static string SCIENCES = "Sciences";
    public static string UPGRADES = "Upgrades";
    public static string FLIGHTS = "Flights";
}
