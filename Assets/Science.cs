﻿using UnityEngine;
using System.Collections;
using System;
using LitJson;

public class Science : MonoBehaviour
{
    public int Id { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public bool InProgress { get; set; }
    public bool CanUpgrade { get; set; }
    public string QueueId { get; set; }
    public string PicUrl { get; set; }

    public Science(JsonData response)
    {
        Id = Int32.Parse(response["id"].ToString());
        Name = response["name"].ToString();
        Level = Int32.Parse(response["level"].ToString());

        //Type = "Science";

        switch (Id)
        {
            case 1:
            case 2:
            case 3:
            case 4:
                Type = "Basic";
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                Type = "Advanced";
                break;
            case 14:
            case 15:
            case 17:
            case 21:
            case 22:
            case 23:
                Type = "Master";
                break;
            case 18:
            case 19:
            case 20:
                Type = "Additional";
                break;
            default:
                break;
        }

        if (response["isInQueue"].ToString() == "0")
        {
            InProgress = false;
        }
        else if (response["isInQueue"].ToString() == "1")
        {
            InProgress = true;
        }
        else
        {
            InProgress = (bool)response["isInQueue"];
        }

        if (response["canUpgrade"].ToString() == "0")
        {
            CanUpgrade = false;
        }
        else if (response["canUpgrade"].ToString() == "1")
        {
            CanUpgrade = true;
        }
        else
        {
            CanUpgrade = (bool)response["canUpgrade"];
        }

        PicUrl = "http://static.nemexia.net/game_dev/img/v2.0/img/technics/science/large/" + Id + ".jpg";

        //Debug.Log(response["cantUpgradeReason"].ToString());
    }

    public void SetQueueId(string id)
    {
        QueueId = id;
    }
}
