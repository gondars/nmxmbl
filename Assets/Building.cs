﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;

public class Building
{
    public int Id { get; set; }
    public string Type { get; set; }
    public string RaceId { get; set; }
    public bool InProgress { get; set; }
    public string Name { get; set; }
    public bool CanUpgrade { get; set; }
    public string CantUpgradeReason { get; set; }
    public bool CanBeDemolished { get; set; }
    public int PicStage { get; set; }
    public int Level { get; set; }
    public string PicUrl { get; set; }

    public string QueueId { get; set; }


    public Building(JsonData response)
    {
        //IDictionary tdictionary = response as IDictionary;
        //if(tdictionary.Contains("in_progress"))
        //{
        //    Debug.Log("IMA");
        //}
        //else
        //{
        //    Debug.Log("NQMA");
        //}


        Id = Int32.Parse(response["building_id"].ToString());

        //Type = Int32.Parse(response["type"].ToString());

        switch (Id)
        {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 23:
                Type = "Resources";
                break;
            case 10:
            case 11:
            case 12:
            case 19:
            case 20:
            case 21:
                Type = "Military";
                break;
            case 22:
            case 24:
            case 25:
            case 26:
            case 27:
                Type = "Alliance";
                break;
            default:
                Type = "KURAC building???"; // id 18...
                break;
        }

        RaceId = response["race_id"].ToString();

        Name = response["name"].ToString();
        CanUpgrade = (bool)response["can_be_upgraded"];
        if (!CanUpgrade)
        {
            CantUpgradeReason = response["cant_upgrade_reason"].ToString();
        }
        CanBeDemolished = (bool)response["can_be_demolished"];
        PicStage = Int32.Parse(response["picture_stage"].ToString());
        Level = Int32.Parse(response["level"].ToString());

        if (response["in_progress"].ToString() == "0")
        {
            InProgress = false;
        }
        else
        {
            InProgress = (bool)response["in_progress"];
        }

        if (Id < 24)
        {
            PicUrl = "http://static.nemexia.net/game/img/v2.0/img/buildings/race" + RaceId + "/thumb_";
            if (Id < 11)
            {
                PicUrl += "1" + Id.ToString();
            }
            else if (Id < 19)
            {
                PicUrl += "2" + Id.ToString();
            }
            else if (Id < 24)
            {
                PicUrl += "3" + Id.ToString();
            }
            PicUrl += ".jpg";
        }
        else
        {
            PicUrl = "http://static.nemexia.net/game/img/v2.0/img/buildings/galaxyzone/thumb_" + Id.ToString() + ".jpg";
        }
    }

    public void SetQueueId(string id)
    {
        QueueId = id;
    }
}