﻿using UnityEngine;
using System.Collections;

public class DynamicListSize : MonoBehaviour
{
    public int childrenHeight;
    void OnEnable()
    {
        float height = 0f;

        foreach (Transform child in transform)
        {
            child.GetComponent<RectTransform>().sizeDelta = new Vector2(child.GetComponent<RectTransform>().rect.width, childrenHeight);
            height += childrenHeight;
        }

        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, height);
    }
}
