﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContainerResizer : MonoBehaviour
{
    public void resizeContainer(float height)
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, height);
    }
}
