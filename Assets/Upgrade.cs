﻿using UnityEngine;
using System.Collections;
using System;
using LitJson;

public class Upgrade : MonoBehaviour
{

    public int Id { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public bool InProgress { get; set; }
    public bool CanUpgrade { get; set; }
    public string PicUrl { get; set; }
    public string QueueId { get; set; }
    public Upgrade(JsonData response)
    {
        Id = Int32.Parse(response["ship_id"].ToString());
        Name = response["name"].ToString();
        Level = Int32.Parse(response["current_level"].ToString());

        //Type = "Upgrade";
        switch (Id)
        {
            case 1:
            case 2:
            case 10:
            case 11:
            case 12:
            case 13:
                Type = "CivilianShips";
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                Type = "WarShips";
                break;
            default:
                break;
        }

        if (response["in_progress"].ToString() == "0")
        {
            InProgress = false;
        }
        else if (response["in_progress"].ToString() == "1")
        {
            InProgress = true;
        }
        else
        {
            InProgress = (bool)response["in_progress"];
        }

        if (response["can_start"].ToString() == "0")
        {
            CanUpgrade = false;
        }
        else if (response["can_start"].ToString() == "1")
        {
            CanUpgrade = true;
        }
        else
        {
            CanUpgrade = (bool)response["can_start"];
        }

        PicUrl = "http://static.nemexia.net/game_dev/img/v2.0/img/ships/race1/thumbs/ship_" + Id + ".png";

    }

    public void SetQueueId(string id)
    {
        QueueId = id;
    }

}
