﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DynamicPopUp : MonoBehaviour
{
    public GameObject mainContainer;
    protected GameObject currCont;
    protected int currentRow = 0;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    
    protected GameObject baseObject;
    protected GameObject overlay;
    protected GameObject containerC;
    protected GameObject background;
    protected GameObject bgBorder;
    private float bgPadding = 40; // total in both sides (bgPadding/2 left, bgPadding/2 right, bgPadding/2 top, bgPadding/2 bot)

    private float lineX = 0;
    private float lineY = 0;

    void Start()
    {
        //Create();
    }

    void Update()
    {
        
    }

    public virtual void Create()
    {
        baseObject = CreatePanel(GameObject.FindGameObjectWithTag("Canvas").transform);
        baseObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        baseObject.AddComponent<Image>();
        baseObject.GetComponent<Image>().color = new Color(0, 0, 0, 0.5f);
        //Debug.Log(baseObject.GetComponent<RectTransform>().position.x);

        overlay = CreatePanel(baseObject.transform);
        overlay.AddComponent<Button>();
        overlay.AddComponent<Image>();
        overlay.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        overlay.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        string helpStr = "helpStr";
        overlay.GetComponent<Button>().onClick.AddListener(() => DestroyObject(helpStr));
        overlay.name = "overlay";
        


        background = CreatePanel(baseObject.transform);
        background.name = "background";


        mainContainer = CreatePanel(baseObject.transform);
        mainContainer.name = "MainContainer";

        //GetBounds();
        //SetBackground();
        //FinishDrawing();
    }

    public GameObject CreatePanel(Transform parent)
    {
        GameObject obj = new GameObject();
        obj.transform.parent = parent;
        obj.AddComponent<RectTransform>();
        obj.AddComponent<CanvasRenderer>();
        obj.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        obj.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        return obj;
    }

    public void SetName(string name)
    {
        baseObject.name = name;
    }

    public void GetBounds(GameObject container)
    {
        if (container.transform.childCount == 0)
        {
            minX = 0;
            maxX = 0;
            minY = 0;
            maxY = 0;
            return;
        }
        minX = container.transform.GetChild(0).GetComponent<RectTransform>().position.x;
        maxX = container.transform.GetChild(0).GetComponent<RectTransform>().position.x + container.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
        maxY = container.transform.GetChild(0).GetComponent<RectTransform>().position.y;
        minY = container.transform.GetChild(0).GetComponent<RectTransform>().position.y - container.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;
        foreach (Transform child in container.transform)
        {
            //if (container == mainContainer)
            //{
            //    Debug.Log("MaxY : " + child.GetComponent<RectTransform>().position.y + " MinY : " + (child.GetComponent<RectTransform>().position.y - child.GetComponent<RectTransform>().sizeDelta.y));
            //}

            if (minX > child.GetComponent<RectTransform>().position.x)
            {
                minX = child.GetComponent<RectTransform>().position.x;
            }
            if (maxX < child.GetComponent<RectTransform>().position.x + child.GetComponent<RectTransform>().sizeDelta.x)
            {
                maxX = child.GetComponent<RectTransform>().position.x + child.GetComponent<RectTransform>().sizeDelta.x;
            }
            // In unity position 0,0 is in Bottom Left corner, but elements drawing is from Top Left bottom, thus the MaxY is the top and MinY is bottom of each element
            // position.y = top of the element | position.y - sizeDelta.y = bottom of element
            if (maxY < child.GetComponent<RectTransform>().position.y)
            {
                maxY = child.GetComponent<RectTransform>().position.y;
            }
            if (minY > child.GetComponent<RectTransform>().position.y - child.GetComponent<RectTransform>().sizeDelta.y)
            {
                minY = child.GetComponent<RectTransform>().position.y - child.GetComponent<RectTransform>().sizeDelta.y;
            }


        }
    }

    public void SetBackground()
    {
        Image img = background.AddComponent<Image>();
        img.sprite = Resources.Load("images/ViewImages/Popup/popupBackground", typeof(Sprite)) as Sprite;
        img.type = Image.Type.Tiled;

        bgBorder = CreatePanel(img.transform);
        Image borderImg = bgBorder.AddComponent<Image>();
        borderImg.sprite = Resources.Load("images/ViewImages/Popup/popupBackgroundBorder", typeof(Sprite)) as Sprite;
        borderImg.type = Image.Type.Sliced;
       
       
    }

    public void FinishDrawing()
    {
        //Debug.Log("END RESULTS! MinX : " + minX + "MaxX : " + maxX + "MinY : " + minY + "MaxY : " + maxY);
        Vector2 bgSize = new Vector2(Mathf.Abs(maxX - minX) + bgPadding, Mathf.Abs(maxY - minY) + bgPadding);
        mainContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(Mathf.Abs(maxX - minX), Mathf.Abs(maxY - minY));
        //GetComponent<RectTransform>().sizeDelta = new Vector2(Mathf.Abs(maxX - minX) + bgPadding, Mathf.Abs(maxY - minY) + bgPadding);
        background.GetComponent<RectTransform>().sizeDelta = bgSize;
        bgBorder.GetComponent<RectTransform>().sizeDelta = bgSize;
        

        background.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        background.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        background.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

        // test working with DynamicPopUpList(1)
        //GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        //GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        //GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);


        mainContainer.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        mainContainer.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        mainContainer.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
    }

    public void AddComponent(GameObject child)
    {
        child.transform.SetParent(mainContainer.transform);
        child.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        child.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        child.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
    }

    protected void AddRow()
    {
        currentRow++;

        GameObject cont = CreatePanel(mainContainer.transform);
        cont.name = "row " + currentRow;

        //Debug.Log(LineY);
        cont.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1);
        cont.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 1);
        cont.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 1);
        cont.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, LineY);

        //Debug.Log("[ADD ROW] MaxY : " + cont.GetComponent<RectTransform>().position.y + " MinY : " + (cont.GetComponent<RectTransform>().position.y - cont.GetComponent<RectTransform>().sizeDelta.y));

        currCont = cont;
    }

    protected void FinishRow()
    {
        GetBounds(currCont);
        currCont.GetComponent<RectTransform>().sizeDelta = new Vector2(Mathf.Abs(maxX - minX), Mathf.Abs(maxY - minY));

        //Debug.Log("[FINISH ROW] MaxY : " + currCont.GetComponent<RectTransform>().position.y + " MinY : " + (currCont.GetComponent<RectTransform>().position.y - currCont.GetComponent<RectTransform>().sizeDelta.y));

        LineX = 0;
        LineY -= currCont.GetComponent<RectTransform>().sizeDelta.y;
    }

    protected void AddRowElement(GameObject go)
    {
        go.transform.SetParent(currCont.transform);
        go.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        go.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        go.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
    }

    protected void AddVerticalPadding(float percHeight = .02f)
    {
        LineY -= Screen.height * percHeight;
    }

    protected float LineX
    {
        get { return lineX; }

        set { lineX = value; }
    }

    protected float LineY
    {
        get { return lineY; }

        set { lineY = value; }
    }

    protected virtual void AddDescription() { }

    protected void DestroyObject(string txt = "")
    {
        //Debug.Log("destroy popup");
        Destroy(baseObject);
    }
}
