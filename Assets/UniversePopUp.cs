﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class UniversePopUp : DynamicPopUp
{
    private int elementsPerRow = 6;
    private Vector2 elementSize = new Vector2(Screen.width * .1f, Screen.width * .1f);
    private float padding = Screen.width * .01f;
    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {

    }
    // Update is called once per frame

    public override void Create()
    {
        base.Create();
        base.SetName("Language Pop-Up");

        AddUniverses();
        GetBounds(mainContainer);
        SetBackground();
        FinishDrawing();
    }

    void AddUniverses()
    {
        int languageCounter = 0;

        string[] worlds = GameManager.instance.universes[NetworkManager.instance.gameId];

        for (int i=0; i < worlds.Length; i++)
        {

            GameObject go = new GameObject();
            go.name = worlds[i];

            Button btn = go.AddComponent<Button>();
            Image img = go.AddComponent<Image>();
            img.color = new Color32(175, 175, 175, 255);

            GameObject textGO = new GameObject();
            textGO.transform.SetParent(go.transform);
            Text txt = textGO.gameObject.AddComponent<Text>();
            txt.text = worlds[i];
            txt.alignment = TextAnchor.MiddleCenter;
            txt.font = Resources.Load<Font>("Fonts/PlayRegular");
            txt.color = Color.black;
            txt.fontSize = 28;
            btn.onClick.AddListener(() => selectLanguage(txt.text));


            go.GetComponent<RectTransform>().sizeDelta = elementSize;
            AddComponent(go);
            double prm = Math.Floor(Convert.ToDouble(languageCounter / elementsPerRow));
            go.GetComponent<RectTransform>().anchoredPosition = new Vector2((languageCounter % elementsPerRow) * (elementSize.x + padding), -(float)prm * (elementSize.y + padding));
            languageCounter++;

        }

    }


    public void selectLanguage(string server = "1")
    {
        NetworkManager.instance.server = server;
        base.DestroyObject();
    }
}
